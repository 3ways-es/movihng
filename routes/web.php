<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificacionesController;
use App\Http\Controllers\DiarioController;
use App\Http\Controllers\ResetPasswordController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('pages.home');
});
*/
Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/programa1', function() {
    return view('pages.programa', ['programa' => 1]);
})->name('pages.programa1');

Route::get('/programa2', function() {
    return view('pages.programa', ['programa' => 2]);
})->name('pages.programa2');

Route::get('/programa3', function() {
    return view('pages.programa', ['programa' => 3]);
})->name('pages.programa3');

Route::get('/programa4', function() {
    return view('pages.programa', ['programa' => 4]);
})->name('pages.programa4');


Route::get('/objetivos', function () {
    return view('pages.objetivos');
});

/*
Route::get('/recuperar-contraseña/{key}/{codigo}', function(){
    return view('pages.login.password-reset', compact('key', 'codigo'));
})->name('reset-password');
*/

Route::get('recuperar-contraseña/{key}/{codigo}', [ResetPasswordController::class, 'infoResetPassword'])->name('reset-password');

Route::post('/change-newpassword', [ResetPasswordController::class, 'resetPassword'])->name('change-newpassword');


Route::get('/contacta', function () {
    return view('pages.contacta');
});

Route::get('/login', function () {
    return view('pages.login.login');
});

Route::post('/login', [LoginController::class, 'authenticate']);

Route::get('/login/{:o?}/{:codigo?}', function ($o = null) {
    return view('pages.login.login');
})->name('login');

Route::post('/nuevapassword', [UserController::class, 'createPassword']);

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', function () {
        return view('pages.principal.principal');
    })->name('principal');

    Route::get('/citas', [UserController::class, 'cargarCalendarioDeCitasHome'])->name('citas');

    Route::get('/logout', [UserController::class, 'logoutUser'])->name('logout.perform');

    Route::get('/perfil', function() {
        return view('pages.perfil.home');
    })->name('perfil');

    Route::get('/perfil/diario-de-la-actividad/{programa}', [UserController::class, 'cargarDiarioDeLaActividad'])->name('perfil.diario-de-la-actividad');

    Route::get('/perfil/calendario-de-citas', [UserController::class, 'cargarCalendarioDeCitas'])->name('perfil.calendario-de-citas');

    Route::post('crearactividad', [UserController::class, 'crearActividad'])->name('perfil.crear-actividad');


    /* Rutas Chat */
    Route::get('/perfil/chat', [App\Http\Controllers\GroupController::class, 'index'])->name('chat');

    Route::resource('groups', App\Http\Controllers\GroupController::class);
    

    Route::resource('conversations', App\Http\Controllers\ConversationController::class);
    Route::post('/getConversation', [App\Http\Controllers\ConversationController::class, 'getConversation']);
    Route::post('/broadcastMessage', [App\Http\Controllers\ConversationController::class, 'broadcastMessage']);
    Route::post('/removeNotifications', [App\Http\Controllers\ConversationController::class, 'removeNotifications']);
    Route::post('/cambiarProgramaUsuario', [App\Http\Controllers\ConversationController::class, 'cambiarProgramaUsuario']);
    Route::post('/getGroupUserIds', [App\Http\Controllers\ConversationController::class, 'getGroupUserIds']);



    /*
    Route::get('/contacto', function () {
        return view('pages.perfil.contacto');
    })->name('perfil.contacto');
    */
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/descargardiario', [DiarioController::class, 'downloadExcelDiario'])->name('descargardiario');

    Route::get('/clear-cache', function() {
        Artisan::call('cache:clear');
        Artisan::call('optimize:clear');
        Artisan::call('route:cache');
        return redirect()->route('voyager.dashboard');
    });
});


/* Rutas ajax*/
Route::group(['prefix' => 'ajax'], function () {
    Route::post('/checkmail', [UserController::class, 'ajaxCheckEmail']);
    Route::post('/changemesdiarioactividad', [UserController::class, 'ajaxChangeMesDiarioActividad']);
    Route::post('/notificacionesusuario', [NotificacionesController::class, 'ajaxObtenerNotificacionesUsuario']);
    Route::post('/obtenerinfousuario', [UserController::class, 'ajaxObtenerInfoUsuario']);
    Route::post('/sendresetpassword', [ResetPasswordController::class, 'ajaxSendResetEmail']);

});