<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index(){
        if(Auth::id())
        {
            $o_user = User::find(Auth::id());
            $o_user->last_login = date('Y-m-d H:i:s');
            $o_user->save();
            return view('pages.principal.principal');
        }
        else
        {
            return view('pages.home');

        }
    }
}
