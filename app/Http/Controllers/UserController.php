<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Diario;
use App\Models\Cita;
use App\Models\Notification;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * 
     * Función que va a checkear si el usuario tiene contraseña
     * 
     * */
    public function ajaxCheckEmail(Request $request)
    {
        // Buscamos el usuario con ese codigo
        $o_user = User::where('codigo', $request->codigo)->first();
        $a_resp['b_error'] = 0;
        if (@$o_user) {
            // Comprobamos si tiene contraseña, si la tiene login si no tiene a introducir su contraseña
            if (@$o_user->password) {
                $a_resp['vista'] = view('pages.login.form-login', ['codigo' => $request->codigo])->render();
            } else {
                $a_resp['vista'] = view('pages.login.form-create-password', ['codigo' => $request->codigo])->render();
            }
        } else {
            $a_resp['b_error'] = 1;
            $a_resp['s_error'] = 'No existe ese codigo';
        }
        return response()->json($a_resp);
    }

    /**
     * 
     * Función para cambiar el mes del diario de actividad
     * 
     * */
    public function ajaxChangeMesDiarioActividad(Request $request)
    {
        $a_resp['b_error'] = 0;
        $a_resp['b_all'] = 0;

        // Comprobamos si tiene alguna semana hecha
        if (Diario::where('user_id', \Auth::id())->where('mes', $request->mes)->exists()) {
            $a_resp['b_all'] = 1;

            // Obtenemos los diarios de actividad de ese mes
            $o_diarios = Diario::where('user_id', \Auth::id())->where('mes', $request->mes)->get();

            // Obtenemos las semanas enviadas
            $a_semanashechas = [
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,
            ];
            foreach ($o_diarios as $index => $value) {
                $a_semanashechas[$value->semana] = 1;
            }
            $a_resp['a_semanashechas'] = $a_semanashechas;
            $a_resp['i_mes'] = $request->mes;
        } else {
            $a_resp['i_mes'] = $request->mes;
            $a_resp['i_semana'] = 1;
        }
        return response()->json($a_resp);
    }

    public function ajaxObtenerInfoUsuario(Request $request)
    {
        // Obtenemos la Cita
        $o_cita = Cita::find(base64_decode($request->info));

        switch (date('l', strtotime($o_cita->fecha_cita))) {
            case 'Monday':
                $semanaEs = 'Lunes';
                break;
            case 'Tuesday':
                $semanaEs = 'Martes';
                break;
            case 'Wednesday':
                $semanaEs = 'Miércoles';
                break;
            case 'Thursday':
                $semanaEs = 'Jueves';
                break;
            case 'Friday':
                $semanaEs = 'Viernes';
                break;
            case 'Saturday':
                $semanaEs = 'Sábado';
                break;
            case 'Sunday':
                $semanaEs = 'Domingo';
                break;
        }

        $a_meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $a_resp['titulo'] = $o_cita->titulo;
        $a_resp['descripcion'] = $o_cita->descripcion;
        $a_resp['color'] = $o_cita->color;
        $a_resp['dia'] = date('d', strtotime($o_cita->fecha_cita));
        $a_resp['mes'] = substr($a_meses[date('m', strtotime($o_cita->fecha_cita)) - 1], 0, 3);
        $a_resp['diasemana'] = $semanaEs;
        $a_resp['hora'] = date('H:i', strtotime($o_cita->fecha_cita));
        return response()->json($a_resp);
    }

    public function createPassword(Request $request)
    {
        $v = \Validator::make($request->all(), [

            'codigo' => 'required',
            'password' => 'required',
            'contrasenianueva' => 'required|same:password',

        ], [
            'codigo.required' => 'Se necesita código',
            'password.required' => 'Introduce una contraseña',
            'contrasenianueva.required' => 'Introduce la confirmación de contraseña',
            'contrasenianueva.same' => 'Las contraseñas no coinciden'
        ]);
        if ($v->fails()) {
            return redirect()->route('login', ['o' => 1, 'codigo' => $request->codigo])->withInput()->withErrors($v->errors());
        }

        $o_user = User::where('codigo', $request->codigo)->first();
        $a_resp['b_error'] = 0;

        if (@$o_user) {
            $o_user->password = bcrypt($request->password);
            $o_user->save();

            $credentials = $request->only('codigo', 'password');

            if (Auth::attempt($credentials)) {
                return redirect()->intended('home');
            }
        } else {
            $a_resp['b_error'] = 1;
            $a_errores['codigo'] = 'Código no valido';
        }

        if ($a_resp['b_error'] == 1) {
            return redirect()->route('login', ['o' => 1, 'codigo' => $request->codigo])->withInput()->withErrors(array('codigo' => @$a_errores['codigo'] ? $a_errores['codigo'] : null));
        }
    }

    public function logoutUser()
    {
        Session::flush();

        Auth::logout();

        return redirect('login');
    }

    public function cargarDiarioDeLaActividad(Request $request)
    {
        //Comprobamos si tiene diario de actividad
        if (Diario::where('user_id', \Auth::id())->exists()) {

            // Obtenemos los diarios del usuario
            $o_diarios = Diario::where('user_id', \Auth::id())->orderBy('mes')->get();

            // Almacenamos los meses y las semanas que tiene hechas

            $a_fechas_disponibles = [
                '1' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '2' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '3' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '4' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '5' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '6' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '7' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '8' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '9' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '10' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '11' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
                '12' => [
                    '1' => 0,
                    '2' => 0,
                    '3' => 0,
                    '4' => 0,
                ],
            ];

            //Obtenemos las semanas y meses registrados
            foreach ($o_diarios as $index => $value) {
                $a_fechas_disponibles[$value->mes][$value->semana] = 1;
            }

            // dd($a_fechas_disponibles);
            $a_meses = array();

            foreach ($a_fechas_disponibles as $index => $fecha_disponible) {
                if ($fecha_disponible[1] == 1 && $fecha_disponible[2] == 1 && $fecha_disponible[3] == 1 && $fecha_disponible[4] == 1) {
                    $a_meses[] = [
                        'valido' => false,
                        'mes' => $index
                    ];
                } else {
                    $a_meses[] = [
                        'valido' => true,
                        'mes' => $index
                    ];

                    if (!@$i_primermes) {
                        $a_semana = [
                            '1' => $fecha_disponible[1],
                            '2' => $fecha_disponible[2],
                            '3' => $fecha_disponible[3],
                            '4' => $fecha_disponible[4],

                        ];

                        $i_primermes = $index;
                    }
                }
            }

            if (@$a_semana) {
                return view(
                    'pages.perfil.diario-de-actividad',
                    [
                        'programa' => $request->programa,
                        'meses' => $a_meses,
                        'semana' => $a_semana,
                        'primermes' => $i_primermes,
                    ]
                )->render();
            } else {
                return redirect()->route('perfil', ['b' => 3]);
            }
        } else {
            $a_semana = [
                '1' => 0,
                '2' => 0,
                '3' => 0,
                '4' => 0,

            ];
            return view(
                'pages.perfil.diario-de-actividad',
                [
                    'programa' => $request->programa,
                    'semana' => $a_semana,
                    'primermes' => 1,
                ]
            )->render();
        }
    }

    public function crearActividad(Request $request)
    {
        $v = \Validator::make($request->all(), [

            'plunes' => 'required',
            'pmartes' => 'required',
            'pmiercoles' => 'required',
            'pjueves' => 'required',
            'pviernes' => 'required',
            'psabado' => 'required',
            'pdomingo' => 'required',
            'calidadsalud' => 'required',
            'esfuerzo' => 'required',
            'adherencia' => 'required',

        ], [
            'esfuerzo.required' => 'Selecciona cuanto esfuerzo te ha supuesto el ejercicio de esta semana',
            'adherencia.required' => 'Seleccione una de las 3 caras',
        ]);
        if ($v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());;
        }

        /* Crear El diario*/
        $o_diario = new Diario;
        $o_diario->user_id = Auth::id();
        $o_diario->programa = $request->programa;
        $o_diario->mes = $request->mes;
        $o_diario->semana = $request->semana;
        $o_diario->pasos_lunes = $request->plunes;
        $o_diario->pasos_martes = $request->pmartes;
        $o_diario->pasos_miercoles = $request->pmiercoles;
        $o_diario->pasos_jueves = $request->pjueves;
        $o_diario->pasos_viernes = $request->pviernes;
        $o_diario->pasos_sabado = $request->psabado;
        $o_diario->pasos_domingo = $request->pdomingo;
        $o_diario->frecuencia_cardiaca = @$request->frecuenciamediasemanal ? $request->frecuenciamediasemanal : 0;
        $o_diario->frecuencia_cardiaca_max_1 = @$request->frecuenciamaxima1 ? $request->frecuenciamaxima1 : 0;
        $o_diario->frecuencia_cardiaca_max_2 = @$request->frecuenciamaxima2 ? $request->frecuenciamaxima2 : 0;
        $o_diario->adherencia = $request->adherencia;
        $o_diario->esfuerzo = $request->esfuerzo;
        $o_diario->calidad_salud = $request->calidadsalud;
        $o_diario->comentario = @$request->comentario ? $request->comentario : null;

        $o_diario->save();

        return redirect()->route('perfil', ['b' => 1]);
    }

    /* Función calendario de citas*/
    public function cargarCalendarioDeCitas()
    {
        $o_citas = Cita::select(
            '*',
            \DB::raw("DATE_FORMAT(fecha_cita,'%M') as months")
        )
            ->where('user_id', Auth::id())
            ->orderBy('fecha_cita')
            ->get()
            ->groupBy('months');

        return view('pages.perfil.calendario-de-citas', ['citas' => $o_citas]);
    }

    public function cargarCalendarioDeCitasHome()
    {
        /* Funcion Citas la campanita*/
        $o_citas = Cita::select(
            '*',
            \DB::raw("DATE_FORMAT(fecha_cita,'%M') as months")
        )
            ->where('user_id', Auth::id())
            ->orderBy('fecha_cita')
            ->get()
            ->groupBy('months');

        /* Marcamos en visto todas las notificaciones */
        $o_notificaciones = Notification::where('user_id', Auth::id())->where('tipo', 1)->where('visto', 0)->update([
            'visto' => 1
        ]);

        return view('pages.citas.calendario-de-citas-home', ['citas' => $o_citas]);
    }
}
