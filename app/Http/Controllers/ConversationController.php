<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\Events\NewMessage;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Group;
use App\Models\GroupUser;
use App\Models\User;


class ConversationController extends Controller
{
  public function store()
  {
    $conversation = Conversation::create([
      'message' => request('message'),
      'group_id' => request('group_id'),
      'user_id' => auth()->user()->id,
    ]);

    $conversation->load('user');

    /* CREAR LA NOTIFICACIÓN*/

    $this->createNotification(request('group_id'));

    broadcast(new NewMessage($conversation))->toOthers();

    return $conversation->load('user');
  }

  public function getConversation(Request $request)
  {
    $conversation = Conversation::where('group_id', $request->group_id)->get();

    $conversation->load('user');

    return $conversation->load('user');
  }

  public function getGroupUserIds(Request $request)
  {
    $group_user_ids = GroupUser::where('group_id', $request->group_id)->where('user_id', '!=', $request->user_id)->get()->pluck('user_id');

    $group_users = User::whereIn('id', $group_user_ids)->get();

    return $group_users;
  }

  public function broadcastMessage()
  {
    $groups_id = Group::all()->pluck('id')->toArray();
    $conversations = [];
    
    foreach ($groups_id as $key => $group_id) {
      
      $conversation = Conversation::create([
        'message' => request('message'),
        'group_id' => $group_id,
        'user_id' => auth()->user()->id,
      ]);

      $conversation->load('user');

      /* CREAR LA NOTIFICACIÓN*/
      $this->createNotification($group_id);

      broadcast(new NewMessage($conversation));

      array_push($conversations, $conversation->load('user'));
    }
    
    return $conversations;
  }


  private function createNotification($group_id){

    $group = Group::where('id', $group_id)->with('users')->first();

    foreach ($group->users as $u) {
      if($u->id != auth()->user()->id){
        $o_notification = new Notification;
        $o_notification->user_id = $u->id;
        $o_notification->group_id = $group_id;
        // Notificacion tipo = 2 ya que es una notificación de chat
        $o_notification->tipo = 2;
        // Notificacion visto = 0 ya que no está vista la notificacion
        $o_notification->visto = 0;
        $o_notification->save();
      }
    }
  }

  public function removeNotifications (Request $request) {
    $success = false;
    $group_id = $request->group_id;
    $user_id = $request->user_id;
    $o_notificaciones = Notification::where('user_id', $user_id)->where('group_id', $group_id)->where('tipo', 2)->where('visto', 0)->update(['visto' => 1 ]);
    $success = true;
    $message = 'Notificaciones eliminadas';

    $response = [$success, $message]; 
    return '';
  }

  public function cambiarProgramaUsuario (Request $request)
  {
    $success = false;
    $o_user = User::find($request->user_id);
    if(auth()->user()->role_id != 2)
    {
      if(@$o_user->programa_id == 0)
      {
        if($request->programa == 1 || $request->programa == 2 || $request->programa == 3 ||  $request->programa == 4 )
        {
          $o_user->programa_id = $request->programa;
          $o_user->save();
          $success = true;
          $message = 'El usuario con código: ' . $o_user->codigo. ' ahora pertenece al programa '. $request->programa;
          $programa = $request->programa;
        }
        else
        {
          $message = 'Solo se puede poner el programa 1, 2, 3 o 4';
        }     
      }
      else
      {
        $message = 'Solo se pueden modificar usuarios cuyo programa sea 0';
      }
    }
    else
    {
      $message = 'No tienes permisos';
    }
  
    $response = [$success, $message, @$programa ? $programa : '']; 

    return response()->json($response);

  }
}
