<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $v = \Validator::make($request->all(), [
            
            'codigo' => 'required',
            'password' => 'required',

        ],[
            'codigo.required' => 'Se necesita código',
            'password.required' => 'Introduce una contraseña',
        ]);
        if ($v->fails())
        {
            return redirect()->route('login', ['o'=>2, 'codigo' => $request->codigo])->withInput()->withErrors($v->errors());
        }

        $credentials = $request->only('codigo', 'password');

        if (Auth::attempt($credentials)) {
            $o_user = User::find(Auth::id());
            $o_user->last_login = date('Y-m-d H:i:s');
            $o_user->save();
            return redirect()->intended('home');
        }
        else{
            return redirect()->route('login', ['o'=>2, 'codigo' => $request->codigo])->withInput()->withErrors(array('codigo'=>'Las credenciales no coinciden'));
        }
    }
}
