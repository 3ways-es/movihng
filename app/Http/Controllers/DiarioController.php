<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DiarioExport;


class DiarioController extends Controller
{
    
    public function downloadExcelDiario(Request $request)
    {
        $o_diario = new DiarioExport('id', $request->id);
        return Excel::download($o_diario,'diariosdeactividad.xlsx');
    }
}
