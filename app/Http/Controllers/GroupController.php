<?php

namespace App\Http\Controllers;

use App\Events\GroupCreated;
use App\Models\Group;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Http\Request;

class GroupController extends Controller
{

  public function index()
  {
    $groups = auth()->user()->groups;

    if(auth()->user()->role_id == 2){
      $users = User::where('role_id', '=', 3)->get();
    }else{
      $users = User::where('id', '<>', auth()->user()->id)->get();
    }
    $user = auth()->user();

    $o_notificaciones = Notification::where('user_id', auth()->user()->id)->where('tipo', 2)->where('visto', 0)->get();
    
    // update(['visto' => 1 ]);

    return view('pages.chat.grupo', ['groups' => $groups, 'users' => $users, 'user' => $user, 'notifications' => $o_notificaciones]);
  }

  public function store()
  {
      $group = Group::create(['name' => request('name')]);
  
      $users = collect(request('users'));
      $users->push(auth()->user()->id);
  
      $group->users()->attach($users);

      broadcast(new GroupCreated($group))->toOthers();
  
      return $group;
  }
}
