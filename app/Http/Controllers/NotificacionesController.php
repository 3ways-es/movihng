<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class NotificacionesController extends Controller
{
    public function ajaxObtenerNotificacionesUsuario(Request $request){
        $o_notificaciones_1 = Notification::where('user_id', Auth::id())->where('tipo', 1)->where('visto', 0)->get()->toArray();
        $o_notificaciones_2 = Notification::where('user_id', Auth::id())->where('tipo', 2)->where('visto', 0)->get()->toArray();
        $a_resp['n_total_1'] = count($o_notificaciones_1);
        $a_resp['n_total_2'] = count($o_notificaciones_2);

        return response()->json($a_resp);
    }
}
