<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Mail\ResetPasswordEmail;
use Illuminate\Support\Facades\Mail;

class ResetPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function ajaxSendResetEmail(Request $request)
    {
        $o_user = User::where('codigo', $request->codigo)->first();
        $a_resp['b_error'] = 0;
        if(@$o_user)
        {
            $data = new \stdClass();
            $data->codigo = $o_user->codigo;
            $data->key = md5($o_user->id . '_&hasdsdn?_');
            Mail::to($o_user->email)->send(new ResetPasswordEmail($data));
        }
        else
        {
            $a_resp['b_error'] = 1;
            $a_resp['s_error'] = 'No existe ese codigo';
        }

        return response()->json($a_resp);
    }

    public function resetPassword(Request $request)
    {
       $v = \Validator::make($request->all(), [
            'key' => 'required',
            'codigo' => 'required',
            'password' => 'required',
            'contrasenianueva' => 'required|same:password',

        ],[
            'codigo.required' => 'Se necesita código',
            'password.required' => 'Introduce una contraseña',
            'contrasenianueva.required' => 'Introduce la confirmación de contraseña',
            'contrasenianueva.same' => 'Las contraseñas no coinciden'
        ]);
        if ($v->fails())
        {
            return redirect()->route('reset-password', ['key'=>$request->key, 'codigo' => $request->codigo])->withInput()->withErrors($v->errors());
        }

        $o_user = User::where('codigo', $request->codigo)->first();
        $a_resp['b_error'] = 0;
        if(@$o_user && $request->key == md5($o_user->id . '_&hasdsdn?_'))
        {
            $o_user->password = bcrypt($request->password);
            $o_user->save();

            return redirect()->route('login', ['o'=>2, 'codigo' => $request->codigo, 'rp' => 1])->withInput()->withErrors($v->errors());
 
        }
        else{
            $a_resp['b_error'] = 1;
            $a_errores['codigo'] = 'Código no valido';
        }

        if($a_resp['b_error'] == 1)
        {
           return redirect()->route('reset-password', ['key'=>$request->key, 'codigo' => $request->codigo])->withInput()->withErrors(array('codigo'=>@$a_errores['codigo'] ? $a_errores['codigo'] : null));
        }
    }

    public function infoResetPassword($key, $codigo)
    {
        return view('pages.login.password-reset', compact('key', 'codigo'));
    }
}