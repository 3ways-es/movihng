<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        \Voyager::addAction(\App\Actions\VerDiariosActividadUsuario::class);
        \Voyager::addAction(\App\Actions\VerCitasUsuario::class);
        \Voyager::addAction(\App\Actions\DescargarExcelDiarios::class);
        \Voyager::addAction(\App\Actions\DescargarTodosLosExcelUsuario::class);
    }
}
