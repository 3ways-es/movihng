<?php
namespace App\Actions;
use TCG\Voyager\Actions\AbstractAction;
class VerCitasUsuario extends AbstractAction
{
    public function getTitle()
    {
        return 'Citas';
    }
    public function getIcon()
    {
        return 'voyager-calendar';
    }
    public function getPolicy()
    {
        return 'read';
    }
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-warning pull-right edit',
        ];
    }
    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'users';
    }
    public function getDefaultRoute()
    {
        return route('voyager.citas.index', [
            'key' => 'user_id',
            'filter' => 'equals',
            's' => $this->data->codigo
        ]);
    }
}