<?php
namespace App\Actions;
use TCG\Voyager\Actions\AbstractAction;
class VerDiariosActividadUsuario extends AbstractAction
{
    public function getTitle()
    {
        return 'Diarios de actividad';
    }
    public function getIcon()
    {
        return 'voyager-documentation';
    }
    public function getPolicy()
    {
        return 'read';
    }
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right edit',
        ];
    }
    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'users';
    }
    public function getDefaultRoute()
    {
        return route('voyager.diarios.index', [
            'key' => 'user_id',
            'filter' => 'equals',
            's' => $this->data->codigo
        ]);
    }
}