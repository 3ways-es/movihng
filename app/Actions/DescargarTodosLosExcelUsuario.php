<?php
namespace App\Actions;
use TCG\Voyager\Actions\AbstractAction;
use App\Exports\DiarioExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\User;

class DescargarTodosLosExcelUsuario extends AbstractAction
{
    public function getTitle()
    {
        return 'Descargar Diarios';
    }
    public function getIcon()
    {
        return 'voyager-documentation';
    }
    public function getPolicy()
    {
        return 'read';
    }
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right',
        ];
    }
    public function shouldActionDisplayOnDataType()
    {
        if(@$_GET['s'])
        {
            return $this->dataType->slug == 'diarios';
        }
    }
    public function getDefaultRoute()
    {
        return route('voyager.diarios.index');
    }
    public function massAction($ids, $comingFrom)
    {
        $a_info = explode('&s=', $comingFrom);
        if(@$a_info[1])
        {
            $o_user = User::where('codigo', $a_info[1])->first();
            $o_diario = new DiarioExport('user_id', $o_user->id);
            return Excel::download($o_diario,'diariosdeactividad.xlsx');
        }
        return redirect($comingFrom);
    }
}