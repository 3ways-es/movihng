<?php
namespace App\Actions;
use TCG\Voyager\Actions\AbstractAction;
class DescargarExcelDiarios extends AbstractAction
{
    public function getTitle()
    {
        return 'Descargar diario';
    }
    public function getIcon()
    {
        return 'voyager-documentation';
    }
    public function getPolicy()
    {
        return 'read';
    }
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right',
        ];
    }
    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'diarios';
    }
    public function getDefaultRoute()
    {
        return route('descargardiario', [
            'id' => $this->data->{$this->data->getKeyName()}
        ]);
    }
}