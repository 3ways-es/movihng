<?php

namespace App\Exports;

use App\Models\Diario;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class DiarioExport implements FromView
{
    protected $campo;
    protected $valor;

    public function __construct($campo, $valor)
    {
        $this->campo = $campo;
        $this->valor = $valor;
    }

    public function view(): View
    {
        $o_diarios = Diario::where($this->campo, $this->valor)->get();
        return view('exports.exceldiarios', [
            'o_diarios' => $o_diarios
        ]);
    }
}