<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diario extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'programa',
        'mes',
        'semana',
        'pasos_lunes',
        'pasos_martes',
        'pasos_miercoles',
        'pasos_jueves',
        'pasos_viernes',
        'pasos_sabado',
        'pasos_domingo',
        'frecuencia_cardiaca',
        'frecuencia_cardiaca_max_1',
        'frecuencia_cardiaca_max_2',
        'adherencia',
        'esfuerzo',
        'calidad_salud',
        'comentario'
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }
}
