/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**************************************!*\
  !*** ./resources/js/pages/perfil.js ***!
  \**************************************/
$(function () {
  $('.btn-adhesion').on('click', function () {
    $('.btn-adhesion').removeClass('answer-radio-checked');
    $(this).addClass('answer-radio-checked');
  });
  $('.btn-esfuerzo').on('click', function () {
    $('.btn-esfuerzo').addClass('color-grisclaro');
    $(this).removeClass('color-grisclaro');
    $(this).addClass('color-green');
  });
  $('.input-pasos').on('keyup', function () {
    var media = 0;

    if ($('#plunes').val().length) {
      media += parseInt($('#plunes').val());
    }

    if ($('#pmartes').val().length) {
      media += parseInt($('#pmartes').val());
    }

    if ($('#pmiercoles').val().length) {
      media += parseInt($('#pmiercoles').val());
    }

    if ($('#pjueves').val().length) {
      media += parseInt($('#pjueves').val());
    }

    if ($('#pviernes').val().length) {
      media += parseInt($('#pviernes').val());
    }

    if ($('#psabado').val().length) {
      media += parseInt($('#psabado').val());
    }

    if ($('#pdomingo').val().length) {
      media += parseInt($('#pdomingo').val());
    }

    $('#pmedia').val(Math.round(media / 7));
  });
  /* Función cuando Cambiamos la semana */

  $('#semanaseleccionada').on('change', function () {
    $('#semana').val($(this).val());
  });
  $('.js-meses #messelecionado').on('change', function () {
    $('#mes').val($(this).val());
    $.ajax({
      url: location.origin + '/ajax/changemesdiarioactividad',
      data: {
        'mes': $(this).val(),
        'programa': $(this).data('programa')
      },
      type: 'post',
      success: function success(response) {
        if (response['b_error'] == 0) {
          $('#mes').val(response['i_mes']);
          /* Quitamos los atributos de selected y disabled */

          $('#semana-1').prop('selected', false);
          $('#semana-2').prop('selected', false);
          $('#semana-3').prop('selected', false);
          $('#semana-4').prop('selected', false);
          $('#semana-1').prop('disabled', false);
          $('#semana-2').prop('disabled', false);
          $('#semana-3').prop('disabled', false);
          $('#semana-4').prop('disabled', false);

          if (response['b_all'] == 0) {
            $('#semana-' + response['i_semana']).prop('selected', true);
            $('#semana').val(response['i_semana']);
          } else {
            var b_primero = 0;

            for (var i = 1; i <= 4; i++) {
              if (response['a_semanashechas'][i] == 1) {
                $('#semana-' + i).prop('disabled', true);
              } else {
                if (b_primero == 0) {
                  $('#semana-' + i).prop('selected', true);
                  $('#semana').val(i);
                  b_primero = 1;
                }
              }
            }
          }
        }
      },
      statusCode: {
        404: function _() {
          console.log('error 404');
        }
      },
      error: function error(x, xs, xt) {}
    });
  });
  $('.last-item .js-cita').on('click', function (event) {
    var info = $(this).data('info');
    $.ajax({
      url: location.origin + '/ajax/obtenerinfousuario',
      data: {
        'info': info
      },
      type: 'post',
      success: function success(response) {
        $('.js-titulo-popup').text();
        $('.js-titulo-popup').text(response['titulo']);
        $('.js-fila-citas').remove();
        $('.js-contenido-popup').append('<div class="js-fila-citas row mt-4"><div class="col-3 mt-3 separador-citas"><p class="mb-0 titulo-popups fechas-calendario-citas text-blue"><b>' + response['dia'] + '</b></p><p class="mb-0 titulo-popups text-blue"><b>' + response['mes'] + '</b></p><p class="mb-0 contenido-popups text-blue"><b>' + response['diasemana'] + '</b></p><p class="mb-0 contenido-popups text-blue"><b>' + response['hora'] + '</b></p></div><div class="col-9 align-self-center"><div class="color-' + response['color'] + ' px-2 py-4"><p class="mb-0 contenido-popups text-white"><b>' + response['descripcion'] + '</b></p></div></div></div>'); //$('#popupcita').modal('show');
      },
      statusCode: {
        404: function _() {
          console.log('error 404');
        }
      },
      error: function error(x, xs, xt) {}
    });
  });
});
/******/ })()
;