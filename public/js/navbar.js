/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**************************************!*\
  !*** ./resources/js/pages/navbar.js ***!
  \**************************************/
$(function () {
  $.ajax({
    url: location.origin + '/ajax/notificacionesusuario',
    data: {
      'tipo': 1
    },
    type: 'post',
    success: function success(response) {
      if (response['n_total_1'] > 0) {
        $('.js-notificaciones').append(' <span class="notificacion text-white font-weight-bold">' + response['n_total_1'] + '</span>');
      }

      if (response['n_total_2'] > 0) {
        $('.js-notificaciones-2').append(' <span id="contador-notificaciones" class="notificacion text-white font-weight-bold">' + response['n_total_2'] + '</span>');
      }
    },
    statusCode: {
      404: function _() {
        console.log('error 404');
      }
    },
    error: function error(x, xs, xt) {}
  });
});
/******/ })()
;