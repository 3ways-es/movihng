<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diarios', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->tinyInteger('programa');
            $table->tinyInteger('mes');
            $table->tinyInteger('semana');
            $table->integer('pasos_lunes');
            $table->integer('pasos_martes');
            $table->integer('pasos_miercoles');
            $table->integer('pasos_jueves');
            $table->integer('pasos_viernes');
            $table->integer('pasos_sabado');
            $table->integer('pasos_domingo');
            $table->integer('frecuencia_cardiaca');
            $table->integer('frecuencia_cardiaca_max_1');
            $table->integer('frecuencia_cardiaca_max_2');
            $table->tinyInteger('adherencia');
            $table->tinyInteger('esfuerzo');
            $table->integer('calidad_salud');
            $table->text('comentario')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diarios');
    }
}
