<!doctype html>
@foreach($o_diarios as $index => $diario)
<table>
    <tbody>
    	<tr>
    		<td colspan="8" style="text-align: center"><b>Mes: {{ $diario->mes }} | Semana {{ $diario->semana }}</b></td>
    	</tr>
        <tr>
            <td colspan="8" style="text-align: center"><b> Fecha de envío de diarío de actividad: {{ date('d/m/Y', strtotime($diario->created_at)) }}</b></td>
        </tr>
    	<tr>
    		<td colspan="8" style="text-align: center"><b>Pasos</b></td>
    	</tr>
    	<tr>
    		<td style="text-align: center">L</td>
    		<td style="text-align: center">M</td>
    		<td style="text-align: center">X</td>
    		<td style="text-align: center">J</td>
    		<td style="text-align: center">V</td>
    		<td style="text-align: center">S</td>
    		<td style="text-align: center">D</td>
    		<td style="text-align: center">Media semanal</td>
    	</tr>
    	@php
    		$media = ($diario->pasos_lunes + $diario->pasos_martes + $diario->pasos_miercoles + $diario->pasos_jueves + $diario->pasos_viernes + $diario->pasos_sabado + $diario->pasos_domingo) / 7
    	@endphp
    	<tr>
    		<td style="text-align: center">{{ $diario->pasos_lunes }}</td>
    		<td style="text-align: center">{{ $diario->pasos_martes }}</td>
    		<td style="text-align: center">{{ $diario->pasos_miercoles }}</td>
    		<td style="text-align: center">{{ $diario->pasos_jueves }}</td>
    		<td style="text-align: center">{{ $diario->pasos_viernes }}</td>
    		<td style="text-align: center">{{ $diario->pasos_sabado }}</td>
    		<td style="text-align: center">{{ $diario->pasos_domingo }}</td>
    		<td style="text-align: center">{{ round($media) }}</td>
    	</tr>
    	<tr>
    		<td colspan="4" style="text-align: center">Frecuencia cardiaca media semanal</td>
    		<td colspan="4" style="text-align: center">Frecuencia cardiaca máxima en los tramos rápidos</td>
    	</tr>
    	<tr>
    		<td colspan="4" style="text-align: center">{{ @$diario->frecuencia_cardiaca ? $diario->frecuencia_cardiaca : 'No introducida' }}</td>
    		<td colspan="2" style="text-align: center">{{ @$diario->frecuencia_cardiaca_max_1 ? $diario->frecuencia_cardiaca_max_1 : 'No introducida' }}</td>
    		<td colspan="2" style="text-align: center">{{ @$diario->frecuencia_cardiaca_max_2 ? $diario->frecuencia_cardiaca_max_2  : 'No introducida' }}</td>
    	</tr>
    	<tr>
    		<td colspan="8" style="text-align: center">Adherencia al ejercicio</td>
    	</tr>
    	<tr>
    		@if ($diario->adherencia == 1)
                <td colspan="8" style="text-align: center">2</td>
    		@elseif($diario->adherencia == 2)
                <td colspan="8" style="text-align: center">1</td>
    		@else
                <td colspan="8" style="text-align: center">0</td>
    		@endif
    	</tr>
    	<tr>
    		<td colspan="8" style="text-align: center">¿Cuánto esfuerzo te ha supuesto el ejercicio de esta semana?</td>
    	</tr>
    	<tr>
    		<td colspan="8" style="text-align: center">{{ $diario->esfuerzo }}</td>
    	</tr>
    	<tr>
    		<td colspan="8" style="text-align: center">¿Cómo crees que ha sido la calidad de tu salud esta semana?</td>
    	</tr>
    	<tr>
    		<td colspan="8" style="text-align: center">{{ $diario->calidad_salud }}</td>
    	</tr>
    	<tr>
    		<td colspan="8" style="text-align: center">¿Quieres comentar algo del programa de entrenamiento de esta semana?</td>
    	</tr>
    	<tr>
    		<td colspan="8" style="text-align: center">{{ @$diario->comentario ? $diario->comentario : '' }}</td>
    	</tr>
    </tbody>
</table>
@endforeach
