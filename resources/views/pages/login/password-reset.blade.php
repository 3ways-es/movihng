@extends('layouts.app')
@section('content')
<div class="fondo-login">
   <div class="container-fluid px-4">
      <div class="row form-login justify-content-center align-items-center">
         <div class="col-12">
            <form action="{{ route('change-newpassword') }}" method="POST" enctype="multipart/form-data">
              @csrf
               <input type="hidden" class="hidden" value="{{ $key }}" id="key"  name="key">
              <div class="form-group mb-0">
                <label for="codigo"></label>
                <input type="text" class="form-control newbuttons text-left py-3" id="codigo" placeholder="Código" value="{{ $codigo }}" name="codigo" readonly>
                @if ($errors->has('codigo'))
                    <div class="alert alert-danger">
                        {{ $errors->first('codigo') }}
                    </div>
                @endif
              </div>
              <div class="form-group mb-0">
                  <label for="password"></label>
                  <input type="password" class="form-control newbuttons text-left py-3" id="password" placeholder="Contraseña" name="password">
                  @if ($errors->has('password'))
                    <div class="alert alert-danger">
                        {{ $errors->first('password') }}
                    </div>
                  @endif
              </div>
              <div class="form-group">
                  <label for="contrasenianueva"></label>
                  <input type="password" class="form-control newbuttons text-left py-3" id="contrasenianueva" placeholder="Confirmar Contraseña" name="contrasenianueva">
                  @if ($errors->has('contrasenianueva'))
                    <div class="alert alert-danger">
                      {{ $errors->first('contrasenianueva') }}
                    </div>
                  @endif
              </div>
              <button type="submit" class="newbuttons d-block text-center color-blue text-white w-100 py-3 border-0">Enviar</button>
            </form>
         </div>
      </div>
   </div>
</div>
@stop