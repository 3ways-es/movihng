<form action="login" method="POST">
  @csrf
  <div class="form-group mb-0">
    <label for="codigo"></label>
    <input type="text" class="form-control newbuttons text-left py-3" id="codigo" placeholder="Código" value="{{ @$_GET['codigo'] ? $_GET['codigo'] : $codigo }}" name="codigo" readonly>
    @if ($errors->has('codigo'))
        <div class="alert alert-danger">
            {{ $errors->first('codigo') }}
        </div>
    @endif
  </div>
  <div class="form-group">
      <label for="password"></label>
      <input type="password" class="form-control newbuttons text-left py-3" id="password" placeholder="Contraseña" name="password">
      @if ($errors->has('password'))
        <div class="alert alert-danger">
            {{ $errors->first('password') }}
        </div>
      @endif
  </div>
  @if(!@$_GET['rp'])
  <div class="mb-3">
    <small class="text-white cursor-pointer" data-toggle="modal" data-target="#resetpasswod">Recuperar contraseña</small>
  </div>
  @endif
   <button type="submit" class="newbuttons d-block text-center color-blue text-white w-100 btn-enviar py-3 border-0">Acceder</button>
</form>