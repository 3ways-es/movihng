@extends('layouts.app')
@section('content')
<div class="fondo-login">
   <div class="container-fluid px-4">
      <div class="row pt-5 position-absolute">
         <div class="col-12 text-left">
            <a class="text-blue" href="{{ url('/') }}"><i class="fas fa-chevron-left"></i></a>
         </div>
      </div>
      <div class="row form-login justify-content-center align-items-center">
         <div class="col-12 p-4">
            @if(!@$_GET['o'])
            <div class="js-primerpaso">
              <div class="form-group">
                <label for="codigo"></label>
                <input type="text" class="form-control newbuttons text-left py-3" id="codigo" placeholder="Código">
                <small class="text-danger py-2 text-error"></small>
              </div>
              <div class="mb-3">
                  <small class="text-white cursor-pointer" data-toggle="modal" data-target="#resetpasswod">Recuperar contraseña</small>
              </div>
              <button type="button" class="newbuttons d-block text-center color-blue text-white w-100 btn-enviar py-3 border-0">Acceder</button>
            </div>
             <div class="js-carga"></div>
            @else
               @if(@$_GET['o'] == 1)
                  @include('pages.login.form-create-password')
               @else
                  @include('pages.login.form-login')
               @endif
            @endif
         </div>
      </div>
   </div>
</div>
@stop

@section('modals')
@include('modals.simple-modal', ['customClass' => 'color-coral', 'modalID' => 'resetpasswod', 'titulo' => '<p class="text-blue titulos-popups mb-0 font-weight-bold">Recuperar Contraseña</p>', 
   'contenido' => '
      <div class="js-form">
         <div class="form-group">
            <input type="text" class="form-control newbuttons text-left py-3 required" id="codigoreset" placeholder="Código de Usuario">
            <small class="text-danger py-2 text-error-reset"></small>
         </div>
         <button type="button" class="newbuttons d-block text-center color-blue text-white w-100 btn-reset-password py-3 border-0">Recuperar contraseña</button>
      </div>
      <div class="js-respuestareset d-none">
         <p class="text-blue titulos-popups font-weight-bold">Le hemos enviado un correo para recuperar la contraseña</p>
         <p class="text-blue contenido-popups">Revise su bandeja de entrada y siga las instrucciones</p>
         <p class="text-blue contenido-popups">Si no recibe el email en unos minutos, por favor, compruebe su bandeja de correo no deseado</p>
      </div>
      '
          ])
@endsection