@extends('layouts.app')
@section('content')
<div class="fondo-contacta">
   <div class="container-fluid px-4">
      <div class="row mb-3 pt-5">
         <div class="col-12 text-left">
            <a class="text-blue" href="{{ url('/') }}"><i class="fas fa-chevron-left"></i></a>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <p class="text-blue titulo mb-0 font-weight-bold">Contacta<br>con nosotros</p>
         </div>
      </div>
      <div class="row mt-3">
         <div class="col-12">
            <a class="btn color-blue text-white w-100" href="mailto:movihng@gmail.com">movihng@gmail.com</a>
         </div>
      </div>
   </div>
   
</div>
@stop