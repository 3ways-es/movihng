@extends('layouts.app')
@section('content')
<div class="fondo-objetivos">
   <div class="container-fluid px-4">
      <div class="row mb-3 pt-5">
         <div class="col-12 text-left">
            <a class="text-blue" href="{{ url('/') }}"><i class="fas fa-chevron-left"></i></a>
         </div>
      </div>
      <div class="row">
         <div class="col-12">
            <p class="text-blue titulo mb-0">Objetivos<br>del estudio</p>
         </div>
      </div>
      <div class="row mt-3">
         <div class="col-12 bloque-texto-objetivos">
             <p class="text-blue text-justify">El objetivo principal del estudio MOVIhNG es
analizar el efecto de un programa de ejercicio
multicomponente personalizado (PEMP) en
condiciones de vida real en la composición
corporal, la función física, la fragilidad y la
calidad de vida de las personas mayores con
VIH y compararlo con el efecto en personas
mayores de la misma edad VIH negativos.</p>
             <p class="text-blue mb-0 text-justify">Son también objetivos del estudio MOVIhNG:</p>
             <ul class="text-blue text-justify pl-4">
                <li>Analizar los efectos de un PEMP en
condiciones de vida real en la edad
biológica, los biomarcadores de función
muscular, en los biomarcadores
inmunológicos y en el microbioma.</li>
                <li>Evaluar la adherencia a un PEMP en
condiciones de vida real con supervisión
estrecha, con supervisión media y sin
supervisión.</li>
             </ul>
         </div>
      </div>
   </div>
   
</div>
@stop