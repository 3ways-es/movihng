@extends('layouts.app', ['home' => 1])
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/principal.css') }}?x={{ rand(5, 15) }}">
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/principal.js') }}?x={{ rand(5, 15) }}"></script>
    <script type="text/javascript" src="{{ asset('js/navbar.js') }}?x={{ rand(5, 15) }}"></script>
@endsection
@section('content')
    <div class="cabecera-principal text-center">
        <div class="row align-items-center justify-content-center m-0">
            <div class="col-8 col-sm-6 col-md-4 col-lg-2 p-0">
                <img src="{{ asset('images/logo.png') }}" align="middle" class="logo-homeprincipal">
            </div>
        </div>
    </div>
    <div class="container w-75 last-item">
        <div class="row mt-3">
            <div class="col-12 color-yellow py-5 px-3 newbuttons container-fluid botones-principal" data-toggle="modal"
                data-target="#comienzo">
                <div class="row">
                    <div class="col-2 pr-0">
                        <img src="{{ asset('images/icons/icon1.svg') }}" align="middle" class="w-100 estandar-iconos">
                    </div>
                    <div class="col-10 align-self-center">
                        <p class="mb-0 text-blue align-self-center text-bloques"><b>Antes de empezar el programa de
                                entrenamiento</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 color-green py-5 px-3 newbuttons container-fluid botones-principal" data-toggle="modal"
                data-target="#entrenamiento">
                <div class="row">
                    <div class="col-2 pr-0">
                        <img src="{{ asset('images/icons/icon2.svg') }}" align="middle" class="w-100 estandar-iconos">
                    </div>
                    <div class="col-10 align-self-center">
                        <p class="mb-0 text-white align-self-center text-bloques"><b>Programas de entrenamiento</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3 ">
            <div class="col-12 color-coral py-5 px-3 newbuttons container-fluid botones-principal" data-toggle="modal"
                data-target="#cronograma">
                <div class="row">
                    <div class="col-2 pr-0">
                        <img src="{{ asset('images/icons/icon3.svg') }}" align="middle" class="w-100 estandar-iconos">
                    </div>
                    <div class="col-10 align-self-center">
                        <p class="mb-0 text-blue align-self-center text-bloques"><b>Cronograma general del estudio</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <a href="{{ route('perfil') }}" style="text-decoration: none" class="w-100">
                <div class="col-12 color-blue py-5 px-3 newbuttons container-fluid botones-principal">
                    <div class="row">
                        <div class="col-2 pr-0">
                            <img src="{{ asset('images/icons/icon-zonapersonal.svg') }}" align="middle"
                                class="w-100 estandar-iconos">
                        </div>
                        <div class="col-10 align-self-center">
                            <p class="mb-0 text-white align-self-center text-bloques"><b>Zona personal privada</b></p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="row mt-3">
            <a href="{{ route('logout.perform') }}" style="text-decoration: none" class="w-100">
                <div class="col-12 color-logout py-5 px-3 newbuttons container-fluid botones-principal">
                    <div class="row">
                        <div class="col-2 pr-0">
                            <img src="{{ asset('images/icons/icon-salir.svg') }}" align="middle"
                                class="w-100 estandar-iconos">
                        </div>
                        <div class="col-10 align-self-center">
                            <p class="mb-0 align-self-center text-blue"><b>Cerrar sesion</b></p>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <!--
           <div class="row mt-3" >
              <div class="col-12 color-blue py-5 px-3 newbuttons container-fluid botones-principal">
                <div class="row">
                    <div class="col-2 pr-0">
                        <img src="{{ asset('images/icons/icon4.svg') }}" align="middle" class="w-100 estandar-iconos">
                    </div>
                    <div class="col-10 align-self-center">
                        <p class="mb-0 text-white align-self-center text-bloques"><b>Acceso al CRD</b></p>
                    </div>
                </div>
              </div>
           </div>
           -->
    </div>
@stop

@section('modals')
    <!-- POPUP Amarillo -->
    @include('modals.modals-principal',
    [
    'customClass' => 'bg-white',
    'modalID' => 'comienzo',
    'footer' => 1,
    'customDialog' => 'especialdesk',
    'coloractivo' => 'yellow',
    'seccion1' => 'Recomendaciones',
    'seccion2' => 'Material a utilizar',
    'contenido_seccion1' => '
    <div class="contenido-seccion-yellow-1">
        <div class="container-fluid p-0">
            <div class="row p-0">
                <div class="col-12 col-md-5">
                    <div class="embed-responsive embed-responsive-1by1">
                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/654922232" frameborder="0"
                            scrolling="no" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-12 col-md-7 align-self-center">
                    <p class="text-blue titulos-popups">Recomendaciones generales para realizar los ejercicios</p>
                    <p class="text-blue contenido-popups mb-0">No dejes de ver y escuchar este vídeo antes de empezar con el
                        programa de entrenamiento. Te explicamos las pautas generales para realizarlo correctamente.</p>
                </div>
            </div>
        </div>
    </div>',
    'contenido_seccion2' => '<div class="contenido-seccion-yellow-2" style="display:none;">
        <div class="row mb-3">
            <div class="col-12">
                <div id="carruselyellow" class="carousel slide" data-interval="false">
                    <div class="carousel-inner">
                        <div class="carousel-item active bg-carrusel">
                            <div class="container-fluid p-0">
                                <div class="row p-0">
                                    <div class="col-12 col-md-5">
                                        <img src="'.asset('images/antesdeempezar/img1.png').'" align="middle"
                                            class="img-fluid mb-3">
                                    </div>
                                    <div class="col-12 col-md-7 align-self-center">
                                        <div class="secciones-carrusel">
                                            <p class="text-blue titulos-popups mb-1">Colchoneta</p>
                                            <p class="text-blue contenido-popups mb-0">Cada uno dispone de una colchoneta
                                                para poder hacer los ejercicios de suelo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item bg-carrusel secciones-carrusel">
                            <div class="container-fluid p-0">
                                <div class="row p-0">
                                    <div class="col-12 col-md-5">
                                        <img src="'.asset('images/antesdeempezar/img2.png').'" align="middle"
                                            class="img-fluid mb-3">
                                    </div>
                                    <div class="col-12 col-md-7 align-self-center">
                                        <div class="secciones-carrusel">
                                            <p class="text-blue titulos-popups mb-1">Cintas elásticas</p>
                                            <p class="text-blue contenido-popups mb-3">Para añadir resistencia a los
                                                ejercicios de fuerza. Cada participante dispondrá de las cintas elásticas
                                                que necesite para realizar los ejercicios a lo largo del programa. Hay 3
                                                cintas: la azul, la verde y la amarilla en función de los Kg de
                                                resistencias. Os daremos a cada uno las que se adecúen a vuestro programa de
                                                ejercicio personalizado.</p>
                                            <img src="'.asset('images/antesdeempezar/img3.png').'" align="middle"
                                                class="img-fluid mb-3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item bg-carrusel">
                            <div class="container-fluid p-0">
                                <div class="row p-0">
                                    <div class="col-12 col-md-5">
                                        <div class="embed-responsive embed-responsive-1by1">
                                            <iframe class="embed-responsive-item"
                                                src="https://player.vimeo.com/video/649118862" frameborder="0"
                                                scrolling="no" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-7 align-self-center">
                                        <div class="secciones-carrusel">
                                            <p class="text-blue titulos-popups mb-1">Pulsera de actividad</p>
                                            <p class="text-blue contenido-popups mb-0">A cada participante se os ha
                                                facilitado la pulsera de actividad Mi Smart Band 6. Es muy importante que la
                                                lleves puesta durante todo el tiempo que dura el programa de entrenamiento
                                                para que puedas tener cada día el registro de tus pasos y otra información
                                                que te facilitará la pulsera. Recuerda bajarte la aplicación.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carruselyellow" role="button" data-slide="prev">
                            <i class="fas fa-arrow-left text-blue" aria-hidden="true"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carruselyellow" role="button" data-slide="next">
                            <i class="fas fa-arrow-right text-blue" aria-hidden="true"></i>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <div class="container-fluid">
                        <div class="fixed-bottom fixed-ol">
                            <ol class="carousel-indicators carousel-indicators-puntos">
                                <li data-target="#carruselyellow" data-slide-to="0" class="active "></li>
                                <li data-target="#carruselyellow" data-slide-to="1"></li>
                                <li data-target="#carruselyellow" data-slide-to="2"></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>'
    ]
    )
    <!-- POPUP Verde-->

    @if (\Auth::user()->role->id != 2)
        @include('modals.modals-principal',
        [
        'customClass' => 'bg-white',
        'footer' => 1,
        'customDialog' => 'especialdesk',
        'modalID' => 'entrenamiento',
        'coloractivo' => 'green',
        'seccion1' => 'Ejercicios pre y post entrenamiento',
        'seccion2' => 'Programas de entrenamiento',
        'contenido_seccion1' => '<div class="contenido-seccion-green-1 last-item">
            <div class="row mb-3">
                <div class="col-12">
                    <div id="carusel1green" class="carousel slide" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active bg-carrusel">
                                <div class="container-fluid p-0">
                                    <div class="row p-0">
                                        <div class="col-12 col-md-7  align-self-center">
                                            <p class="text-blue contenido-popups mb-0">Es muy importante preparar nuestro
                                                cuerpo para realizar el programa de entrenamiento. Por eso hay que hacer
                                                <b>unos minutos de calentamiento antes de empezar y unos minutos de
                                                    estiramientos al terminar. Hazlos suavemente, sin forzar, manteniendo la
                                                    posición unos segundos (3-6 segundos). No pivotes, mantén la posición
                                                    fija.</b>
                                            </p>
                                            <p class="text-blue titulos-popups mb-1">Calentamiento</p>
                                            <img src="'.asset('images/entrenamiento/img_calentamiento.png').'"
                                                align="middle" class="w-100 img-calentamiento">
                                        </div>
                                        <div class="col-12 col-md-5">
                                            <div class="secciones-carrusel">
                                                <div class="embed-responsive embed-responsive-1by1">
                                                    <iframe class="embed-responsive-item"
                                                        src="https://player.vimeo.com/video/680351165" frameborder="0"
                                                        scrolling="no" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item bg-carrusel">
                                <div class="container-fluid p-0">
                                    <div class="row p-0">
                                        <div class="col-12 col-md-7  align-self-center">
                                            <p class="text-blue contenido-popups mb-0">Es muy importante preparar nuestro
                                                cuerpo para realizar el programa de entrenamiento. Por eso hay que hacer
                                                unos minutos de calentamiento antes de empezar y unos minutos de
                                                estiramientos al terminar. Hazlos suavemente, sin forzar, manteniendo la
                                                posición unos segundos (3-6 segundos). No pivotes, mantén la posición fija.
                                            </p>
                                        </div>
                                        <div class="col-12 col-md-5">
                                            <div class="secciones-carrusel">
                                                <p class="text-blue titulos-popups mb-1">Estiramiento</p>
                                                <img src="'.asset('images/entrenamiento/img_estiramiento.png').'"
                                                    align="middle" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="fixed-bottom fixed-ol">
                                <ol class="carousel-indicators carousel-indicators-puntos">
                                    <li data-target="#carusel1green" data-slide-to="0" class="active "></li>
                                    <li data-target="#carusel1green" data-slide-to="1"></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>',

        'contenido_seccion2' => '
        <div class="contenido-seccion-green-2" style="display:none;">
            <div class="row mb-3">
                <div class="col-12">
                    <div id="carusel2green" class="carousel slide" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active bg-carrusel">
                                <div class="container-fluid p-0">
                                    <div class="row p-0">
                                        <div class="col-12 col-md-5">
                                            <img src="'.asset('images/entrenamiento/programa1/principal.png').'"
                                                align="middle" class="img-fluid">
                                        </div>
                                        <div class="col-12 col-md-7 align-self-center mb-5">
                                            <div class="secciones-carrusel">
                                                <p class="text-blue titulos-popups mb-1">Programa 1</p>
                                                <div class="color-yellow py-4 px-3 newbuttons botones-principal mb-2 popups"
                                                    data-toggle="modal" data-target="#pm1">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-1.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 align-self-center text-blue contenido-popups ">
                                                                    <b>Planificación mensual</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="color-coral py-4 px-3 newbuttons botones-principal mb-2 popups"
                                                    data-toggle="modal" data-target="#ce1">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-3.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 text-blue align-self-center  contenido-popups ">
                                                                    <b>Circuitos de ejercicios</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="color-blue py-4 px-3 newbuttons botones-principal mb-3 popups"
                                                    data-toggle="modal" data-target="#ap1">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-5.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos ">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 text-white align-self-center text-blue contenido-popups ">
                                                                    <b>Ejercicio aeróbico</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item bg-carrusel">
                                <div class="container-fluid p-0">
                                    <div class="row p-0">
                                        <div class="col-12 col-md-5">
                                            <img src="'.asset('images/entrenamiento/programa2/principal.png').'"
                                                align="middle" class="img-fluid">
                                        </div>
                                        <div class="col-12 col-md-7 align-self-center mb-5">
                                            <div class="secciones-carrusel">
                                                <p class="text-blue titulos-popups mb-1">Programa 2</p>
                                                <div class="color-green py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                                    data-toggle="modal" data-target="#pm2">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-2.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 align-self-center text-blue contenido-popups ">
                                                                    <b>Planificación mensual</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="color-coral py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                                    data-toggle="modal" data-target="#ce2">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-3.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 text-blue align-self-center  contenido-popups ">
                                                                    <b>Circuitos de ejercicios</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="color-blue py-4 px-3 newbuttons d-flex botones-principal mb-3 popups"
                                                    data-toggle="modal" data-target="#ap2">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-5.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos ">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 text-white align-self-center text-blue contenido-popups ">
                                                                    <b>Ejercicio aeróbico</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item bg-carrusel">
                                <div class="container-fluid p-0">
                                    <div class="row p-0">
                                        <div class="col-12 col-md-5">
                                            <img src="'.asset('images/entrenamiento/programa3/principal.png').'"
                                                align="middle" class="img-fluid">
                                        </div>
                                        <div class="col-12 col-md-7 align-self-center mb-5">
                                            <div class="secciones-carrusel">
                                                <p class="text-blue titulos-popups mb-1">Programa 3</p>
                                                <div class="color-yellow py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                                    data-toggle="modal" data-target="#pm3">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-1.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 align-self-center text-blue contenido-popups ">
                                                                    <b>Planificación mensual</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="color-green py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                                    data-toggle="modal" data-target="#ce3">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-4.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 text-blue align-self-center  contenido-popups ">
                                                                    <b>Circuitos de ejercicios</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="color-blue py-4 px-3 newbuttons d-flex botones-principal mb-3 popups"
                                                    data-toggle="modal" data-target="#ap3">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-5.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos ">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 text-white align-self-center text-blue contenido-popups ">
                                                                    <b>Ejercicio aeróbico</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item bg-carrusel">
                                <div class="container-fluid p-0">
                                    <div class="row p-0">
                                        <div class="col-12 col-md-5">
                                            <img src="'.asset('images/entrenamiento/programa4/principal.png').'"
                                                align="middle" class="img-fluid">
                                        </div>
                                        <div class="col-12 col-md-7 align-self-center mb-5">
                                            <div class="secciones-carrusel">
                                                <p class="text-blue titulos-popups mb-1">Programa 4</p>
                                                <div class="color-coral py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                                    data-toggle="modal" data-target="#pm4">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-1.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 align-self-center text-blue contenido-popups ">
                                                                    <b>Planificación mensual</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="color-yellow py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                                    data-toggle="modal" data-target="#ce4">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-4.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 text-blue align-self-center  contenido-popups ">
                                                                    <b>Circuitos de ejercicios</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="color-green py-4 px-3 newbuttons d-flex botones-principal mb-3 popups"
                                                    data-toggle="modal" data-target="#ap4">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <div class="col-2 pr-0">
                                                                <img src="'.asset('images/icons/icon-programas-5.svg').'"
                                                                    align="middle" class="w-100 estandar-iconos ">
                                                            </div>
                                                            <div class="col-10 align-self-center">
                                                                <p
                                                                    class="mb-0 text-white align-self-center text-blue contenido-popups ">
                                                                    <b>Ejercicio aeróbico</b>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carusel2green" role="button" data-slide="prev">
                                <i class="fas fa-arrow-left text-blue" aria-hidden="true"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carusel2green" role="button" data-slide="next">
                                <i class="fas fa-arrow-right text-blue" aria-hidden="true"></i>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div class="container-fluid">
                            <div class="fixed-bottom fixed-ol">
                                <ol class="carousel-indicators carousel-indicators-puntos">
                                    <li data-target="#carusel2green" data-slide-to="0" class="active "></li>
                                    <li data-target="#carusel2green" data-slide-to="1"></li>
                                    <li data-target="#carusel2green" data-slide-to="2"></li>
                                    <li data-target="#carusel2green" data-slide-to="3"></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>'
        ]
        )
        <!-- Planificación Mensual -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'modalID' => 'pm1',
        'titulo' => 'Programa 1',
        'footer' => 1,
        'customDialog' => 'especialdesk',
        'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="1" data-mes="1">
                    <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-1-1"><b>Mes 1</b></p>
                </div>
                <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="1" data-mes="2">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-1-2"><b>Mes 2</b></p>
                </div>
                <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="1" data-mes="3">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-1-3"><b>Mes 3</b></p>
                </div>
            </div>
        </div>
        <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
            <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
            <p class="mb-0 contenido-popups text-white">Mar</p>
            <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
            <p class="mb-0 contenido-popups text-white">Jue</p>
            <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
            <p class="mb-0 contenido-popups text-white">Sáb</p>
            <p class="mb-0 contenido-popups text-white">Dom</p>
        </div>
        <div class="js-mes-1-1">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1 ">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
            <div class="container-fluid mb-4">
                <div class="row">
                    <div class="col-5 explicacion py-2">
                        <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                        <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                        <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                        <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                    </div>
                    <div class="col-7">
                        <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe
                                realizar en este día.</b></p>
                        <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que se
                                realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los segundos sobre
                                cada pie.</b></p>
                        <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso entre
                                cada ejercicio.</b></p>
                        <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que debemos
                                realizar el circuito de ejercicios completo.</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-mes-1-2" style="display: none;">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                    <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                    <div class="container-fluid p-0">
                        <div class="row p-0">
                            <div class="col-4">
                                <div class="color-coral d-block px-2 py-1">
                                    <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                    <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                        repeticiones</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="color-green d-block px-2 py-1">
                                    <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                    <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                        repeticiones</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="color-yellow d-block px-2 py-1">
                                    <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                    <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                        repeticiones</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                    <div class="container-fluid p-0">
                        <div class="row p-0">
                            <div class="col-4">
                                <div class="color-green d-block px-2 py-1">
                                    <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                    <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                        repeticiones</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="color-yellow d-block px-2 py-1">
                                    <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                    <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 -
                                        15repeticiones</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="color-coral d-block px-2 py-1">
                                    <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                    <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                        repeticiones</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 text-right mt-2">
                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-mes-1-3" style="display: none;">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1 ">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">

                        <div class="color-green d-block px-2 py-1 w-100">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1 w-100">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1 w-100">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'modalID' => 'pm2',
        'titulo' => 'Programa 2',
        'footer' => 1,
        'customDialog' => 'especialdesk',
        'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="2" data-mes="1">
                    <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-2-1"><b>Mes 1</b></p>
                </div>
                <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="2" data-mes="2">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-2-2"><b>Mes 2</b></p>
                </div>
                <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="2" data-mes="3">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-2-3"><b>Mes 3</b></p>
                </div>
            </div>
        </div>
        <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
            <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
            <p class="mb-0 contenido-popups text-white">Mar</p>
            <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
            <p class="mb-0 contenido-popups text-white">Jue</p>
            <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
            <p class="mb-0 contenido-popups text-white">Sáb</p>
            <p class="mb-0 contenido-popups text-white">Dom</p>
        </div>
        <div class="js-mes-2-1">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">

                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">

                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1 ">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">

                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>

            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">

                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
            <div class="container-fluid mb-4">
                <div class="row">
                    <div class="col-5 explicacion py-2">
                        <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                        <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                        <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                        <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                    </div>
                    <div class="col-7">
                        <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe
                                realizar en este día.</b></p>
                        <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que se
                                realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los segundos sobre
                                cada pie.</b></p>
                        <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso entre
                                cada ejercicio.</b></p>
                        <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que debemos
                                realizar el circuito de ejercicios completo.</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-mes-2-2" style="display: none;">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>

            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>

            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>

            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
        </div>
        <div class="js-mes-2-3" style="display: none;">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1 ">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'modalID' => 'pm3',
        'titulo' => 'Programa 3',
        'customDialog' => 'especialdesk',
        'footer' => 1,
        'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="3" data-mes="1">
                    <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-3-1"><b>Mes 1</b></p>
                </div>
                <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="3" data-mes="2">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-3-2"><b>Mes 2</b></p>
                </div>
                <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="3" data-mes="3">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-3-3"><b>Mes 3</b></p>
                </div>
            </div>
        </div>
        <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
            <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
            <p class="mb-0 contenido-popups text-white">Mar</p>
            <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
            <p class="mb-0 contenido-popups text-white">Jue</p>
            <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
            <p class="mb-0 contenido-popups text-white">Sáb</p>
            <p class="mb-0 contenido-popups text-white">Dom</p>
        </div>
        <div class="js-mes-3-1">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1 ">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
            <div class="container-fluid mb-4">
                <div class="row">
                    <div class="col-5 explicacion py-2">
                        <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                        <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                        <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                        <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                    </div>
                    <div class="col-7">
                        <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe
                                realizar en este día.</b></p>
                        <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que se
                                realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los segundos sobre
                                cada pie.</b></p>
                        <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso entre
                                cada ejercicio.</b></p>
                        <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que debemos
                                realizar el circuito de ejercicios completo.</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-mes-3-2" style="display: none;">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
        </div>
        <div class="js-mes-3-3" style="display: none;">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1 ">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'modalID' => 'pm4',
        'titulo' => 'Programa 4',
        'customDialog' => 'especialdesk',
        'footer' => 1,
        'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="4" data-mes="1">
                    <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-4-1"><b>Mes 1</b></p>
                </div>
                <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="4" data-mes="2">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-4-2"><b>Mes 2</b></p>
                </div>
                <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="4" data-mes="3">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-4-3"><b>Mes 3</b></p>
                </div>
            </div>
        </div>
        <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
            <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
            <p class="mb-0 contenido-popups text-white">Mar</p>
            <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
            <p class="mb-0 contenido-popups text-white">Jue</p>
            <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
            <p class="mb-0 contenido-popups text-white">Sáb</p>
            <p class="mb-0 contenido-popups text-white">Dom</p>
        </div>
        <div class="js-mes-4-1">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1 ">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
            <div class="container-fluid mb-4">
                <div class="row">
                    <div class="col-5 explicacion py-2">
                        <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                        <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                        <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                        <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                    </div>
                    <div class="col-7">
                        <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe
                                realizar en este día.</b></p>
                        <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que se
                                realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los segundos sobre
                                cada pie.</b></p>
                        <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso entre
                                cada ejercicio.</b></p>
                        <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que debemos
                                realizar el circuito de ejercicios completo.</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-mes-4-2" style="display: none;">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
        </div>
        <div class="js-mes-4-3" style="display: none;">
            <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1 ">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 text-right mt-2">
                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
            </div>
        </div>'
        ]
        )
        <!-- FIN Planificación Mensual -->

        <!-- EJERCICIO AERÓBICO -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'modalID' => 'ap1',
        'titulo' => 'Programa 1',
        'customDialog' => 'especialdesk',
        'footer' => 1,
        'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
        <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
            <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
        </div>
        <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000 pasos</b>
        </p>
        <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b></p>
                </div>
            </div>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'modalID' => 'ap2',
        'titulo' => 'Programa 2',
        'customDialog' => 'especialdesk',
        'footer' => 1,
        'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
        <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
            <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
        </div>
        <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000 pasos</b>
        </p>
        <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b></p>
                </div>
            </div>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'modalID' => 'ap3',
        'titulo' => 'Programa 3',
        'customDialog' => 'especialdesk',
        'footer' => 1,
        'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
        <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
            <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
        </div>
        <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000 pasos</b>
        </p>
        <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b></p>
                </div>
            </div>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'modalID' => 'ap4',
        'titulo' => 'Programa 4',
        'customDialog' => 'especialdesk',
        'footer' => 1,
        'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
        <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
            <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
        </div>
        <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000 pasos</b>
        </p>
        <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-3 p-0">
                    <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                        <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                    </div>
                </div>
                <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al
                            menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b></p>
                </div>
            </div>
        </div>'
        ]
        )
        <!-- FIN EJERCICIO AERÓBICO -->

        <!-- Circuitos de ejercicios 1 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce1',
        'titulo' => 'Programa 1',
        'footer' => 1,
        'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
        <div class="container-fluid p-0">
            <div class="row p-0">
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa1/ejercicio1.png').'" align="middle"
                        class="w-100 mb-3">
                    <button class="color-green newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button"
                        data-toggle="collapse" data-target="#circuito11" aria-expanded="false"
                        aria-controls="circuito11">Ejercicios Circuito 1</button>
                    <div class="collapse multi-collapse" id="circuito11">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce111"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce112"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce113"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce114"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce115"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce116"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce117"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa1/ejercicio2.png').'" align="middle"
                        class="w-100 mb-3">
                    <button class="color-green newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button"
                        data-toggle="collapse" data-target="#circuito12" aria-expanded="false"
                        aria-controls="circuito12">Ejercicios Circuito 2</button>
                    <div class="collapse multi-collapse" id="circuito12">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce121"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce122"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce123"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce124"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce125"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce126"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce127"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa1/ejercicio3.png').'" align="middle"
                        class="w-100 mb-3">
                    <button class="color-green newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button"
                        data-toggle="collapse" data-target="#circuito13" aria-expanded="false"
                        aria-controls="circuito13">Ejercicios Circuito 3</button>
                    <div class="collapse multi-collapse" id="circuito13">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce131"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce132"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce133"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce134"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce135"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce136"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce137"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        '
        ]
        )
        <!-- Circuito 1 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce111',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026029" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce112',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026032" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce113',
        'titulo' => 'Pecho',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026060" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce114',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026096" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce115',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026115" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce116',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026150" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce117',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026180" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 1 -->

        <!-- Circuito 2 -->

        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce121',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032659" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce122',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032701" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce123',
        'titulo' => 'Pecho',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032723" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce124',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032744" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce125',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032766" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce126',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032792" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce127',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032822" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 2 -->

        <!-- Circuito 3 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce131',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046523" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce132',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046570" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce133',
        'titulo' => 'Pecho',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046624" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce134',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046667" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce135',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046700" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce136',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046783" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce137',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046785" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 3 -->
        <!-- fin Circuitos de ejercicios 1 -->

        <!-- Circuitos de ejercicios 2 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce2',
        'titulo' => 'Programa 2',
        'footer' => 1,
        'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
        <div class="container-fluid p-0">
            <div class="row p-0">
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa2/ejercicio1.png').'" align="middle"
                        class="w-100 mb-3">
                    <button class="color-yellow newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                        type="button" data-toggle="collapse" data-target="#circuito21" aria-expanded="false"
                        aria-controls="circuito21">Ejercicios Circuito 1</button>
                    <div class="collapse multi-collapse" id="circuito21">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce211"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce212"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce213"><b>Hombro</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce214"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce215"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce216"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce217"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa2/ejercicio2.png').'" align="middle"
                        class="w-100 mb-3">
                    <button class="color-yellow newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                        type="button" data-toggle="collapse" data-target="#circuito22" aria-expanded="false"
                        aria-controls="circuito22">Ejercicios Circuito 2</button>
                    <div class="collapse multi-collapse" id="circuito22">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce221"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce222"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce223"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce224"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce225"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce226"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce227"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa2/ejercicio3.png').'" align="middle"
                        class="w-100 mb-3">
                    <button class="color-yellow newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                        type="button" data-toggle="collapse" data-target="#circuito23" aria-expanded="false"
                        aria-controls="circuito23">Ejercicios Circuito 3</button>
                    <div class="collapse multi-collapse" id="circuito23">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce231"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce232"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce233"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce234"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce235"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce236"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce237"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        '
        ]
        )
        <!-- Circuito 1 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce211',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085824" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce212',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085928" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce213',
        'titulo' => 'Hombro',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085918" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce214',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085968" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce215',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649086003" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce216',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649086116" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce217',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649086089" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 1 -->

        <!-- Circuito 2 -->

        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce221',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649094982" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce222',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095072" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce223',
        'titulo' => 'Pecho',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095073" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce224',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095117" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce225',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095158" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>' ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce226',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095217" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce227',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095251" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 2 -->

        <!-- Circuito 3 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce231',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100788" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce232',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100860" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce233',
        'titulo' => 'Pecho',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100875" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce234',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100940" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce235',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100972" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce236',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649101006" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce237',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649101058" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 3 -->
        <!-- Fin Circuitos de ejercicios 2 -->

        <!-- Circuitos de ejercicios 3 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce3',
        'titulo' => 'Programa 3',
        'footer' => 1,
        'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
        <div class="container-fluid p-0">
            <div class="row p-0">
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa3/ejercicio1.png').'" align="middle"
                        class="w-100 mb-2">
                    <button class="color-coral newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button"
                        data-toggle="collapse" data-target="#circuito31" aria-expanded="false"
                        aria-controls="circuito31">Ejercicios Circuito 1</button>
                    <div class="collapse multi-collapse" id="circuito31">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce311"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce312"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce313"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce314"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce315"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce316"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce317"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa3/ejercicio2.png').'" align="middle"
                        class="w-100 mb-2">
                    <button class="color-coral newbuttons w-100 mb-3 py-3 border-0 text-blue" type="button"
                        data-toggle="collapse" data-target="#circuito32" aria-expanded="false"
                        aria-controls="circuito32">Ejercicios Circuito 2</button>
                    <div class="collapse multi-collapse" id="circuito32">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce321"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce322"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce323"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce324"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce325"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce326"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce327"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa3/ejercicio3.png').'" align="middle"
                        class="w-100  mb-2">
                    <button class="color-coral newbuttons w-100 mb-3 py-3 border-0 text-blue" type="button"
                        data-toggle="collapse" data-target="#circuito33" aria-expanded="false"
                        aria-controls="circuito33">Ejercicios Circuito 3</button>
                    <div class="collapse multi-collapse" id="circuito33">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce331"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce332"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce333"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce334"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce335"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce336"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce337"><b>Hombro</b></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        '
        ]
        )
        <!-- Circuito 1 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce311',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104751" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce312',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104547" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce313',
        'titulo' => 'Pecho',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104682" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce314',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104793" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce315',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104655" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce316',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104511" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce317',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104611" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 1 -->

        <!-- Circuito 2 -->

        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce321',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111255" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce322',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111065" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce323',
        'titulo' => 'Pecho',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111208" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce324',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111316" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce325',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111161" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce326',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111017" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce327',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111108" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 2 -->

        <!-- Circuito 3 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce331',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115863" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce332',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115654" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce333',
        'titulo' => 'Hombro',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115924" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce334',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115913" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce335',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115755" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce336',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115618" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce337',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115701" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 3 -->

        <!-- fin Circuitos de ejercicios 3-->


        <!-- Circuitos de ejercicios 4 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce4',
        'titulo' => 'Programa 4',
        'footer' => 1,
        'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
        <div class="container-fluid p-0">
            <div class="row p-0">
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa4/ejercicio1.png').'" align="middle"
                        class="w-100 mb-2">
                    <button class="color-blue newbuttons w-100 mb-3 py-3 border-0 text-white cursor-pointer" type="button"
                        data-toggle="collapse" data-target="#circuito41" aria-expanded="false"
                        aria-controls="circuito41">Ejercicios Circuito 1</button>
                    <div class="collapse multi-collapse" id="circuito41">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce411"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce412"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce413"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce414"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce415"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce416"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce417"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa4/ejercicio2.png').'" align="middle"
                        class="w-100 mb-2">
                    <button class="color-blue newbuttons w-100 mb-3 py-3 border-0 text-white" type="button"
                        data-toggle="collapse" data-target="#circuito32" aria-expanded="false"
                        aria-controls="circuito32">Ejercicios Circuito 2</button>
                    <div class="collapse multi-collapse" id="circuito32">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce421"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce422"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce423"><b>Hombro</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce424"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce425"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce426"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce427"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-3">
                    <img src="'.asset('images/entrenamiento/programa4/ejercicio3.png').'" align="middle"
                        class="w-100  mb-2">
                    <button class="color-blue newbuttons w-100 mb-3 py-3 border-0 text-white" type="button"
                        data-toggle="collapse" data-target="#circuito33" aria-expanded="false"
                        aria-controls="circuito33">Ejercicios Circuito 3</button>
                    <div class="collapse multi-collapse" id="circuito33">
                        <ol class="pl-4">
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce431"><b>Sentadillas</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce432"><b>Equilibrio</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce433"><b>Pecho</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce434"><b>Tronco inferior</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce435"><b>Glúteo</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce436"><b>Abdomen</b></li>
                            <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                data-target="#ce437"><b>Espalda</b></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        '
        ]
        )
        <!-- Circuito 1 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce411',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705206" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce412',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705224" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce413',
        'titulo' => 'Pecho',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705243" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce414',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705265" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce415',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705285" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce416',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705307" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce417',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705328" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 1 -->

        <!-- Circuito 2 -->

        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce421',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705344" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce422',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705360" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce423',
        'titulo' => 'Hombro',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705380" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce424',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705393" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce425',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705409" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce426',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705418" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce427',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705430" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 2 -->

        <!-- Circuito 3 -->
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce431',
        'titulo' => 'Sentadillas',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705455" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce432',
        'titulo' => 'Equilibrio',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705474" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce433',
        'titulo' => 'Pecho',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705489" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce434',
        'titulo' => 'Tronco inferior',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705521" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce435',
        'titulo' => 'Glúteo',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705535" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce436',
        'titulo' => 'Abdomen',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705549" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        @include('modals.simple-modal-principal',
        [
        'customClass' => 'bg-white',
        'customDialog' => 'especialdesk',
        'modalID' => 'ce437',
        'titulo' => 'Espalda',
        'footer' => 1,
        'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705571" frameborder="0"
                scrolling="no" allowfullscreen></iframe>
        </div>'
        ]
        )
        <!-- FIN Circuito 4 -->

        <!-- fin Circuitos de ejercicios 4-->
    @else

        @if (\Auth::user()->programa_id == 1)
            @include('modals.modals-principal',
            [
            'customClass' => 'bg-white',
            'footer' => 1,
            'customDialog' => 'especialdesk',
            'modalID' => 'entrenamiento',
            'coloractivo' => 'green',
            'seccion1' => 'Ejercicios pre y post entrenamiento',
            'seccion2' => 'Programas de entrenamiento',
            'contenido_seccion1' => '<div class="contenido-seccion-green-1 last-item">
                <div class="row mb-3">
                    <div class="col-12">
                        <div id="carusel1green" class="carousel slide" data-interval="false">
                            <div class="carousel-inner">
                                <div class="carousel-item active bg-carrusel">
                                    <div class="container-fluid p-0">
                                        <div class="row p-0">
                                            <div class="col-12 col-md-7  align-self-center">
                                                <p class="text-blue contenido-popups mb-0">Es muy importante preparar
                                                    nuestro cuerpo para realizar el programa de entrenamiento. Por eso hay
                                                    que hacer <b>unos minutos de calentamiento antes de empezar y unos
                                                        minutos de estiramientos al terminar. Hazlos suavemente, sin forzar,
                                                        manteniendo la posición unos segundos (3-6 segundos). No pivotes,
                                                        mantén la posición fija.</b></p>
                                            </div>
                                            <div class="col-12 col-md-5">
                                                <div class="secciones-carrusel">
                                                    <p class="text-blue titulos-popups mb-1">Calentamiento</p>
                                                    <img src="'.asset('images/entrenamiento/img_calentamiento.png').'"
                                                        align="middle" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item bg-carrusel">
                                    <div class="container-fluid p-0">
                                        <div class="row p-0">
                                            <div class="col-12 col-md-7  align-self-center">
                                                <p class="text-blue contenido-popups mb-0">Es muy importante preparar
                                                    nuestro cuerpo para realizar el programa de entrenamiento. Por eso hay
                                                    que hacer unos minutos de calentamiento antes de empezar y unos minutos
                                                    de estiramientos al terminar. Hazlos suavemente, sin forzar, manteniendo
                                                    la posición unos segundos (3-6 segundos). No pivotes, mantén la posición
                                                    fija.</p>
                                            </div>
                                            <div class="col-12 col-md-5">
                                                <div class="secciones-carrusel">
                                                    <p class="text-blue titulos-popups mb-1">Estiramiento</p>
                                                    <img src="'.asset('images/entrenamiento/img_estiramiento.png').'"
                                                        align="middle" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <div class="fixed-bottom fixed-ol">
                                    <ol class="carousel-indicators carousel-indicators-puntos">
                                        <li data-target="#carusel1green" data-slide-to="0" class="active "></li>
                                        <li data-target="#carusel1green" data-slide-to="1"></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>',

            'contenido_seccion2' => '<div class="contenido-seccion-green-2 last-item" style="display:none;">
                <div class="row mb-3">
                    <div class="col-12">
                        <div class="container-fluid p-0">
                            <div class="row p-0">
                                <div class="col-12 col-md-5">
                                    <img src="'.asset('images/entrenamiento/programa1/principal.png').'" align="middle"
                                        class="img-fluid">
                                </div>
                                <div class="col-12 col-md-7 align-self-center ">
                                    <div class="secciones-carrusel">
                                        <p class="text-blue titulos-popups mb-1">Programa 1</p>
                                        <div class="color-yellow py-4 px-3 newbuttons botones-principal mb-2 popups"
                                            data-toggle="modal" data-target="#pm1">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-1.svg').'"
                                                            align="middle" class="w-100 estandar-iconos">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p class="mb-0 align-self-center text-blue contenido-popups ">
                                                            <b>Planificación mensual</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="color-coral py-4 px-3 newbuttons botones-principal mb-2 popups"
                                            data-toggle="modal" data-target="#ce1">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-3.svg').'"
                                                            align="middle" class="w-100 estandar-iconos">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p class="mb-0 text-blue align-self-center  contenido-popups ">
                                                            <b>Circuitos de ejercicios</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="color-blue py-4 px-3 newbuttons botones-principal mb-3 popups"
                                            data-toggle="modal" data-target="#ap1">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-5.svg').'"
                                                            align="middle" class="w-100 estandar-iconos ">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p
                                                            class="mb-0 text-white align-self-center text-blue contenido-popups ">
                                                            <b>Ejercicio aeróbico</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>'
            ]
            )
            <!-- Planificación mensual -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'modalID' => 'pm1',
            'titulo' => 'Programa 1',
            'footer' => 1,
            'customDialog' => 'especialdesk',
            'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="1" data-mes="1">
                        <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-1-1"><b>Mes 1</b></p>
                    </div>
                    <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="1" data-mes="2">
                        <p class="mb-3 text-blue contenido-popups js-mesactivo-1-2"><b>Mes 2</b></p>
                    </div>
                    <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="1" data-mes="3">
                        <p class="mb-3 text-blue contenido-popups js-mesactivo-1-3"><b>Mes 3</b></p>
                    </div>
                </div>
            </div>
            <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
                <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
                <p class="mb-0 contenido-popups text-white">Mar</p>
                <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
                <p class="mb-0 contenido-popups text-white">Jue</p>
                <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
                <p class="mb-0 contenido-popups text-white">Sáb</p>
                <p class="mb-0 contenido-popups text-white">Dom</p>
            </div>
            <div class="js-mes-1-1">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1 ">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
                <div class="container-fluid mb-4">
                    <div class="row">
                        <div class="col-5 explicacion py-2">
                            <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                            <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                            <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                            <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                        </div>
                        <div class="col-7">
                            <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe
                                    realizar en este día.</b></p>
                            <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que
                                    se realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los
                                    segundos sobre cada pie.</b></p>
                            <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso
                                    entre cada ejercicio.</b></p>
                            <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que
                                    debemos realizar el circuito de ejercicios completo.</b></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="js-mes-1-2" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                    <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                    <div class="container-fluid p-0">
                        <div class="row p-0">
                            <div class="col-4">
                                <div class="color-yellow d-block px-2 py-1">
                                    <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b>
                                    </p>
                                    <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                        repeticiones</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="color-coral d-block px-2 py-1">
                                    <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b>
                                    </p>
                                    <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                        repeticiones</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="color-green d-block px-2 py-1">
                                    <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b>
                                    </p>
                                    <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                        repeticiones</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                </div>
                            </div>
                        </div>
                        <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                        <div class="container-fluid p-0">
                            <div class="row p-0">
                                <div class="col-4">
                                    <div class="color-coral d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito
                                                3</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                            repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso
                                        </p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas
                                        </p>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="color-green d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito
                                                1</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                            repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso
                                        </p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas
                                        </p>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="color-yellow d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito
                                                2</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                            repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso
                                        </p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                        <div class="container-fluid p-0">
                            <div class="row p-0">
                                <div class="col-4">
                                    <div class="color-green d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito
                                                1</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                            repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso
                                        </p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas
                                        </p>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="color-yellow d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito
                                                2</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 -
                                            15repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso
                                        </p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas
                                        </p>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="color-coral d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito
                                                3</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15
                                            repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso
                                        </p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w-100 text-right mt-2">
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="js-mes-1-3" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1 ">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">

                            <div class="color-green d-block px-2 py-1 w-100">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1 w-100">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1 w-100">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
            </div>'
            ]
            )
            <!-- fin Planificación mensual -->
            <!-- Circuitos de ejercicios -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce1',
            'titulo' => 'Programa 1',
            'footer' => 1,
            'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa1/ejercicio1.png').'" align="middle"
                            class="w-100 mb-3">
                        <button class="color-green newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                            type="button" data-toggle="collapse" data-target="#circuito11" aria-expanded="false"
                            aria-controls="circuito11">Ejercicios Circuito 1</button>
                        <div class="collapse multi-collapse" id="circuito11">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce11"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce12"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce13"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce14"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce15"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce16"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce17"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa1/ejercicio2.png').'" align="middle"
                            class="w-100 mb-3">
                        <button class="color-green newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                            type="button" data-toggle="collapse" data-target="#circuito12" aria-expanded="false"
                            aria-controls="circuito12">Ejercicios Circuito 2</button>
                        <div class="collapse multi-collapse" id="circuito12">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce21"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce22"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce23"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce24"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce25"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce26"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce27"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa1/ejercicio3.png').'" align="middle"
                            class="w-100 mb-3">
                        <button class="color-green newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                            type="button" data-toggle="collapse" data-target="#circuito13" aria-expanded="false"
                            aria-controls="circuito13">Ejercicios Circuito 3</button>
                        <div class="collapse multi-collapse" id="circuito13">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce31"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce32"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce33"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce34"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce35"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce36"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce37"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            '
            ]
            )
            <!-- Circuito 1 -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce11',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026029" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce12',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026032" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce13',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026060" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce14',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026096" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce15',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026115" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce16',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026150" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce17',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026180" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 1 -->

            <!-- Circuito 2 -->

            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce21',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032659" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce22',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032701" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce23',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032723" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce24',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032744" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce25',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032766" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce26',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032792" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce27',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032822" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 2 -->

            <!-- Circuito 3 -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce31',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046523" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce32',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046570" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce33',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046624" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce34',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046667" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce35',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046700" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce36',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046783" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce37',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046785" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 3 -->
            <!-- fin Circuitos de ejercicios -->

            <!-- Ejercicios aerobicos -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'modalID' => 'ap1',
            'titulo' => 'Programa 1',
            'customDialog' => 'especialdesk',
            'footer' => 1,
            'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
            <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
                <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
            </div>
            <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000
                    pasos</b></p>
            <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b>
                        </p>
                    </div>
                </div>
            </div>'
            ]
            )
            <!-- Fin Ejercicios aerobicos -->

        @elseif(\Auth::user()->programa_id == 2)
            @include('modals.modals-principal',
            [
            'customClass' => 'bg-white',
            'footer' => 1,
            'customDialog' => 'especialdesk',
            'modalID' => 'entrenamiento',
            'coloractivo' => 'green',
            'seccion1' => 'Ejercicios pre y post entrenamiento',
            'seccion2' => 'Programas de entrenamiento',
            'contenido_seccion1' => '<div class="contenido-seccion-green-1 contenido-seccion-green-1">
                <div class="row mb-3">
                    <div class="col-12">
                        <div id="carusel1green" class="carousel slide" data-interval="false">
                            <div class="carousel-inner">
                                <div class="carousel-item active bg-carrusel">
                                    <div class="container-fluid p-0">
                                        <div class="row p-0">
                                            <div class="col-12 col-md-7 align-self-center">
                                                <p class="text-blue contenido-popups mb-0">Es muy importante preparar
                                                    nuestro cuerpo para realizar el programa de entrenamiento. Por eso hay
                                                    que hacer <b>unos minutos de calentamiento antes de empezar y unos
                                                        minutos de estiramientos al terminar. Hazlos suavemente, sin forzar,
                                                        manteniendo la posición unos segundos (3-6 segundos). No pivotes,
                                                        mantén la posición fija.</b></p>
                                                <p class="text-blue titulos-popups mb-1">Calentamiento</p>
                                                <img src="'.asset('images/entrenamiento/img_calentamiento.png').'"
                                                    align="middle" class="w-100 img-calentamiento">
                                            </div>
                                            <div class="col-12 col-md-5">
                                                <div class="secciones-carrusel">
                                                    <div class="embed-responsive embed-responsive-1by1">
                                                        <iframe class="embed-responsive-item"
                                                            src="https://player.vimeo.com/video/680351165" frameborder="0"
                                                            scrolling="no" allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item bg-carrusel">
                                    <div class="container-fluid p-0">
                                        <div class="row p-0">
                                            <div class="col-12 col-md-7 align-self-center">
                                                <p class="text-blue contenido-popups mb-0">Es muy importante preparar
                                                    nuestro cuerpo para realizar el programa de entrenamiento. Por eso hay
                                                    que hacer unos minutos de calentamiento antes de empezar y unos minutos
                                                    de estiramientos al terminar. Hazlos suavemente, sin forzar, manteniendo
                                                    la posición unos segundos (3-6 segundos). No pivotes, mantén la posición
                                                    fija.</p>
                                            </div>
                                            <div class="col-12 col-md-5">
                                                <div class="secciones-carrusel">
                                                    <p class="text-blue titulos-popups mb-1">Estiramiento</p>
                                                    <img src="'.asset('images/entrenamiento/img_estiramiento.png').'"
                                                        align="middle" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <div class="fixed-bottom fixed-ol">
                                    <ol class="carousel-indicators carousel-indicators-puntos">
                                        <li data-target="#carusel1green" data-slide-to="0" class="active "></li>
                                        <li data-target="#carusel1green" data-slide-to="1"></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>',

            'contenido_seccion2' => '<div class="contenido-seccion-green-2 last-item" style="display:none;">
                <div class="row mb-3">
                    <div class="col-12">
                        <div class="container-fluid p-0">
                            <div class="row p-0">
                                <div class="col-12 col-md-5">
                                    <img src="'.asset('images/entrenamiento/programa2/principal.png').'" align="middle"
                                        class="img-fluid">
                                </div>
                                <div class="col-12 col-md-7 align-self-center">
                                    <div class="secciones-carrusel">
                                        <p class="text-blue titulos-popups mb-1">Programa 2</p>
                                        <div class="color-green py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                            data-toggle="modal" data-target="#pm2">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-2.svg').'"
                                                            align="middle" class="w-100 estandar-iconos">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p class="mb-0 align-self-center text-blue contenido-popups ">
                                                            <b>Planificación mensual</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="color-coral py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                            data-toggle="modal" data-target="#ce2">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-3.svg').'"
                                                            align="middle" class="w-100 estandar-iconos">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p class="mb-0 text-blue align-self-center  contenido-popups ">
                                                            <b>Circuitos de ejercicios</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="color-blue py-4 px-3 newbuttons d-flex botones-principal mb-3 popups"
                                            data-toggle="modal" data-target="#ap2">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-5.svg').'"
                                                            align="middle" class="w-100 estandar-iconos ">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p
                                                            class="mb-0 text-white align-self-center text-blue contenido-popups ">
                                                            <b>Ejercicio aeróbico</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>'
            ]
            )
            <!-- Planificación mensual -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'modalID' => 'pm2',
            'titulo' => 'Programa 2',
            'footer' => 1,
            'customDialog' => 'especialdesk',
            'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="2" data-mes="1">
                        <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-2-1"><b>Mes 1</b></p>
                    </div>
                    <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="2" data-mes="2">
                        <p class="mb-3 text-blue contenido-popups js-mesactivo-2-2"><b>Mes 2</b></p>
                    </div>
                    <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="2" data-mes="3">
                        <p class="mb-3 text-blue contenido-popups js-mesactivo-2-3"><b>Mes 3</b></p>
                    </div>
                </div>
            </div>
            <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
                <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
                <p class="mb-0 contenido-popups text-white">Mar</p>
                <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
                <p class="mb-0 contenido-popups text-white">Jue</p>
                <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
                <p class="mb-0 contenido-popups text-white">Sáb</p>
                <p class="mb-0 contenido-popups text-white">Dom</p>
            </div>
            <div class="js-mes-2-1">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">

                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">

                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1 ">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">

                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">

                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
                <div class="container-fluid mb-4">
                    <div class="row">
                        <div class="col-5 explicacion py-2">
                            <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                            <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                            <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                            <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                        </div>
                        <div class="col-7">
                            <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe
                                    realizar en este día.</b></p>
                            <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que
                                    se realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los
                                    segundos sobre cada pie.</b></p>
                            <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso
                                    entre cada ejercicio.</b></p>
                            <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que
                                    debemos realizar el circuito de ejercicios completo.</b></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="js-mes-2-2" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>

                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
            </div>
            <div class="js-mes-2-3" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1 ">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
            </div>'
            ]
            )
            <!-- fin Planificación mensual -->

            <!-- Circuitos de ejercicios -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce2',
            'titulo' => 'Programa 2',
            'footer' => 1,
            'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa2/ejercicio1.png').'" align="middle"
                            class="w-100 mb-3">
                        <button class="color-yellow newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                            type="button" data-toggle="collapse" data-target="#circuito21" aria-expanded="false"
                            aria-controls="circuito21">Ejercicios Circuito 1</button>
                        <div class="collapse multi-collapse" id="circuito21">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce11"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce12"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce13"><b>Hombro</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce14"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce15"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce16"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce17"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa2/ejercicio2.png').'" align="middle"
                            class="w-100 mb-3">
                        <button class="color-yellow newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                            type="button" data-toggle="collapse" data-target="#circuito22" aria-expanded="false"
                            aria-controls="circuito22">Ejercicios Circuito 2</button>
                        <div class="collapse multi-collapse" id="circuito22">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce21"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce22"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce23"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce24"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce25"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce26"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce27"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa2/ejercicio3.png').'" align="middle"
                            class="w-100 mb-3">
                        <button class="color-yellow newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                            type="button" data-toggle="collapse" data-target="#circuito23" aria-expanded="false"
                            aria-controls="circuito23">Ejercicios Circuito 3</button>
                        <div class="collapse multi-collapse" id="circuito23">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce31"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce32"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce33"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce34"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce35"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce36"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce37"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            '
            ]
            )
            <!-- Circuito 1 -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce11',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085824" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce12',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085928" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce13',
            'titulo' => 'Hombro',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085918" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce14',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085968" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce15',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649086003" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce16',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649086116" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce17',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649086089" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 1 -->

            <!-- Circuito 2 -->

            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce21',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649094982" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce22',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095072" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce23',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095073" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce24',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095117" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce25',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095158" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>' ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce26',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095217" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce27',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095251" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 2 -->

            <!-- Circuito 3 -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce31',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100788" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce32',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100860" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce33',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100875" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce34',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100940" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce35',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100972" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce36',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649101006" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce37',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649101058" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 3 -->
            <!-- Fin Circuitos de ejercicios 2-->

            <!-- Ejercicios aerobicos -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'modalID' => 'ap2',
            'titulo' => 'Programa 2',
            'customDialog' => 'especialdesk',
            'footer' => 1,
            'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
            <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
                <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
            </div>
            <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000
                    pasos</b></p>
            <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b>
                        </p>
                    </div>
                </div>
            </div>'
            ]
            )
            <!-- Fin Ejercicios aerobicos -->
        @elseif(\Auth::user()->programa_id == 3)
            @include('modals.modals-principal',
            [
            'customClass' => 'bg-white',
            'footer' => 1,
            'customDialog' => 'especialdesk',
            'modalID' => 'entrenamiento',
            'coloractivo' => 'green',
            'seccion1' => 'Ejercicios pre y post entrenamiento',
            'seccion2' => 'Programas de entrenamiento',
            'contenido_seccion1' => '<div class="contenido-seccion-green-1 last-item">
                <div class="row mb-3">
                    <div class="col-12">
                        <div id="carusel1green" class="carousel slide" data-interval="false">
                            <div class="carousel-inner">
                                <div class="carousel-item active bg-carrusel">
                                    <div class="container-fluid p-0">
                                        <div class="row p-0">
                                            <div class="col-12 col-md-7 align-self-center">
                                                <p class="text-blue contenido-popups mb-0">Es muy importante preparar
                                                    nuestro cuerpo para realizar el programa de entrenamiento. Por eso hay
                                                    que hacer <b>unos minutos de calentamiento antes de empezar y unos
                                                        minutos de estiramientos al terminar. Hazlos suavemente, sin forzar,
                                                        manteniendo la posición unos segundos (3-6 segundos). No pivotes,
                                                        mantén la posición fija.</b></p>
                                                <p class="text-blue titulos-popups mb-1">Calentamiento</p>
                                                <img src="'.asset('images/entrenamiento/img_calentamiento.png').'"
                                                    align="middle" class="w-100 img-calentamiento">
                                            </div>
                                            <div class="col-12 col-md-5">
                                                <div class="secciones-carrusel">
                                                    <div class="embed-responsive embed-responsive-1by1">
                                                        <iframe class="embed-responsive-item"
                                                            src="https://player.vimeo.com/video/680351165" frameborder="0"
                                                            scrolling="no" allowfullscreen></iframe>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item bg-carrusel">
                                    <div class="container-fluid p-0">
                                        <div class="row p-0">
                                            <div class="col-12 col-md-7 align-self-center">
                                                <p class="text-blue contenido-popups mb-0">Es muy importante preparar
                                                    nuestro cuerpo para realizar el programa de entrenamiento. Por eso hay
                                                    que hacer unos minutos de calentamiento antes de empezar y unos minutos
                                                    de estiramientos al terminar. Hazlos suavemente, sin forzar, manteniendo
                                                    la posición unos segundos (3-6 segundos). No pivotes, mantén la posición
                                                    fija.</p>
                                            </div>
                                            <div class="col-12 col-md-5">
                                                <div class="secciones-carrusel">
                                                    <p class="text-blue titulos-popups mb-1">Estiramiento</p>
                                                    <img src="'.asset('images/entrenamiento/img_estiramiento.png').'"
                                                        align="middle" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <div class="fixed-bottom fixed-ol">
                                    <ol class="carousel-indicators carousel-indicators-puntos">
                                        <li data-target="#carusel1green" data-slide-to="0" class="active "></li>
                                        <li data-target="#carusel1green" data-slide-to="1"></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>',

            'contenido_seccion2' => '<div class="contenido-seccion-green-2 last-item" style="display:none;">
                <div class="row mb-3">
                    <div class="col-12">
                        <div class="container-fluid p-0">
                            <div class="row p-0">
                                <div class="col-12 col-md-5">
                                    <img src="'.asset('images/entrenamiento/programa3/principal.png').'" align="middle"
                                        class="img-fluid">
                                </div>
                                <div class="col-12 col-md-7 align-self-center">
                                    <div class="secciones-carrusel">
                                        <p class="text-blue titulos-popups mb-1">Programa 3</p>
                                        <div class="color-yellow py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                            data-toggle="modal" data-target="#pm3">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-1.svg').'"
                                                            align="middle" class="w-100 estandar-iconos">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p class="mb-0 align-self-center text-blue contenido-popups ">
                                                            <b>Planificación mensual</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="color-green py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                            data-toggle="modal" data-target="#ce3">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-4.svg').'"
                                                            align="middle" class="w-100 estandar-iconos">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p class="mb-0 text-blue align-self-center  contenido-popups ">
                                                            <b>Circuitos de ejercicios</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="color-blue py-4 px-3 newbuttons d-flex botones-principal mb-3 popups"
                                            data-toggle="modal" data-target="#ap3">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-5.svg').'"
                                                            align="middle" class="w-100 estandar-iconos ">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p
                                                            class="mb-0 text-white align-self-center text-blue contenido-popups ">
                                                            <b>Ejercicio aeróbico</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>'
            ]
            )
            <!-- Planificación mensual -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'modalID' => 'pm3',
            'titulo' => 'Programa 3',
            'customDialog' => 'especialdesk',
            'footer' => 1,
            'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="3" data-mes="1">
                        <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-3-1"><b>Mes 1</b></p>
                    </div>
                    <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="3" data-mes="2">
                        <p class="mb-3 text-blue contenido-popups js-mesactivo-3-2"><b>Mes 2</b></p>
                    </div>
                    <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="3" data-mes="3">
                        <p class="mb-3 text-blue contenido-popups js-mesactivo-3-3"><b>Mes 3</b></p>
                    </div>
                </div>
            </div>
            <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
                <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
                <p class="mb-0 contenido-popups text-white">Mar</p>
                <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
                <p class="mb-0 contenido-popups text-white">Jue</p>
                <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
                <p class="mb-0 contenido-popups text-white">Sáb</p>
                <p class="mb-0 contenido-popups text-white">Dom</p>
            </div>
            <div class="js-mes-3-1">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1 ">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
                <div class="container-fluid mb-4">
                    <div class="row">
                        <div class="col-5 explicacion py-2">
                            <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                            <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                            <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                            <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                        </div>
                        <div class="col-7">
                            <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe
                                    realizar en este día.</b></p>
                            <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que
                                    se realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los
                                    segundos sobre cada pie.</b></p>
                            <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso
                                    entre cada ejercicio.</b></p>
                            <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que
                                    debemos realizar el circuito de ejercicios completo.</b></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="js-mes-3-2" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
            </div>
            <div class="js-mes-3-3" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1 ">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
            </div>'
            ]
            )
            <!-- fin Planificación mensual -->

            <!-- Circuitos de ejercicios -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce3',
            'titulo' => 'Programa 3',
            'footer' => 1,
            'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa3/ejercicio1.png').'" align="middle"
                            class="w-100 mb-2">
                        <button class="color-coral newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer"
                            type="button" data-toggle="collapse" data-target="#circuito31" aria-expanded="false"
                            aria-controls="circuito31">Ejercicios Circuito 1</button>
                        <div class="collapse multi-collapse" id="circuito31">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce11"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce12"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce13"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce14"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce15"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce16"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce17"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa3/ejercicio2.png').'" align="middle"
                            class="w-100 mb-2">
                        <button class="color-coral newbuttons w-100 mb-3 py-3 border-0 text-blue" type="button"
                            data-toggle="collapse" data-target="#circuito32" aria-expanded="false"
                            aria-controls="circuito32">Ejercicios Circuito 2</button>
                        <div class="collapse multi-collapse" id="circuito32">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce21"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce22"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce23"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce24"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce25"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce26"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce27"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa3/ejercicio3.png').'" align="middle"
                            class="w-100  mb-2">
                        <button class="color-coral newbuttons w-100 mb-3 py-3 border-0 text-blue" type="button"
                            data-toggle="collapse" data-target="#circuito33" aria-expanded="false"
                            aria-controls="circuito33">Ejercicios Circuito 3</button>
                        <div class="collapse multi-collapse" id="circuito33">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce31"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce32"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce33"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce34"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce35"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce36"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce37"><b>Hombro</b></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            '
            ]
            )
            <!-- Circuito 1 -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce11',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104751" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce12',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104547" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce13',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104682" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce14',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104793" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce15',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104655" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce16',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104511" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce17',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104611" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 1 -->

            <!-- Circuito 2 -->

            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce21',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111255" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce22',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111065" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce23',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111208" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce24',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111316" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce25',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111161" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce26',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111017" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce27',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111108" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 2 -->

            <!-- Circuito 3 -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce31',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115863" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce32',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115654" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce33',
            'titulo' => 'Hombro',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115924" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce34',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115913" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce35',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115755" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce36',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115618" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce37',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115701" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 3 -->

            <!-- fin Circuitos de ejercicios -->

            <!-- Ejercicios aerobicos -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'modalID' => 'ap3',
            'titulo' => 'Programa 3',
            'customDialog' => 'especialdesk',
            'footer' => 1,
            'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
            <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
                <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
            </div>
            <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000
                    pasos</b></p>
            <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b>
                        </p>
                    </div>
                </div>
            </div>'
            ]
            )
            <!-- Fin Ejercicios aerobicos -->
        @else
            @include('modals.modals-principal',
            [
            'customClass' => 'bg-white',
            'footer' => 1,
            'customDialog' => 'especialdesk',
            'modalID' => 'entrenamiento',
            'coloractivo' => 'green',
            'seccion1' => 'Ejercicios pre y post entrenamiento',
            'seccion2' => 'Programas de entrenamiento',
            'contenido_seccion1' => '<div class="contenido-seccion-green-1 last-item">
                <div class="row mb-3">
                    <div class="col-12">
                        <div id="carusel1green" class="carousel slide" data-interval="false">
                            <div class="carousel-inner">
                                <div class="carousel-item active bg-carrusel">
                                    <div class="container-fluid p-0">
                                        <div class="row p-0">
                                            <div class="col-12 col-md-7 align-self-center">
                                                <p class="text-blue contenido-popups mb-0">Es muy importante preparar
                                                    nuestro cuerpo para realizar el programa de entrenamiento. Por eso hay
                                                    que hacer <b>unos minutos de calentamiento antes de empezar y unos
                                                        minutos de estiramientos al terminar. Hazlos suavemente, sin forzar,
                                                        manteniendo la posición unos segundos (3-6 segundos). No pivotes,
                                                        mantén la posición fija.</b></p>
                                                <p class="text-blue titulos-popups mb-1">Calentamiento</p>
                                                <img src="'.asset('images/entrenamiento/img_calentamiento.png').'"
                                                    align="middle" class="w-100 img-calentamiento">
                                            </div>
                                            <div class="col-12 col-md-5">
                                                <div class="secciones-carrusel">
                                                    <div class="embed-responsive embed-responsive-1by1">
                                                        <iframe class="embed-responsive-item"
                                                            src="https://player.vimeo.com/video/680351165" frameborder="0"
                                                            scrolling="no" allowfullscreen></iframe>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item bg-carrusel">
                                    <div class="container-fluid p-0">
                                        <div class="row p-0">
                                            <div class="col-12 col-md-7 align-self-center">
                                                <p class="text-blue contenido-popups mb-0">Es muy importante preparar
                                                    nuestro cuerpo para realizar el programa de entrenamiento. Por eso hay
                                                    que hacer unos minutos de calentamiento antes de empezar y unos minutos
                                                    de estiramientos al terminar. Hazlos suavemente, sin forzar, manteniendo
                                                    la posición unos segundos (3-6 segundos). No pivotes, mantén la posición
                                                    fija.</p>
                                            </div>
                                            <div class="col-12 col-md-5">
                                                <div class="secciones-carrusel">
                                                    <p class="text-blue titulos-popups mb-1">Estiramiento</p>
                                                    <img src="'.asset('images/entrenamiento/img_estiramiento.png').'"
                                                        align="middle" class="img-fluid">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container-fluid">
                                <div class="fixed-bottom fixed-ol">
                                    <ol class="carousel-indicators carousel-indicators-puntos">
                                        <li data-target="#carusel1green" data-slide-to="0" class="active "></li>
                                        <li data-target="#carusel1green" data-slide-to="1"></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>',

            'contenido_seccion2' => '<div class="contenido-seccion-green-2 last-item" style="display:none;">
                <div class="row mb-3">
                    <div class="col-12">
                        <div class="container-fluid p-0">
                            <div class="row p-0">
                                <div class="col-12 col-md-5">
                                    <img src="'.asset('images/entrenamiento/programa4/principal.png').'" align="middle"
                                        class="img-fluid">
                                </div>
                                <div class="col-12 col-md-7 align-self-center">
                                    <div class="secciones-carrusel">
                                        <p class="text-blue titulos-popups mb-1">Programa 4</p>
                                        <div class="color-yellow py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                            data-toggle="modal" data-target="#pm4">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-1.svg').'"
                                                            align="middle" class="w-100 estandar-iconos">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p class="mb-0 align-self-center text-blue contenido-popups ">
                                                            <b>Planificación mensual</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="color-green py-4 px-3 newbuttons d-flex botones-principal mb-2 popups"
                                            data-toggle="modal" data-target="#ce4">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-4.svg').'"
                                                            align="middle" class="w-100 estandar-iconos">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p class="mb-0 text-blue align-self-center  contenido-popups ">
                                                            <b>Circuitos de ejercicios</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="color-blue py-4 px-3 newbuttons d-flex botones-principal mb-3 popups"
                                            data-toggle="modal" data-target="#ap4">
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-2 pr-0">
                                                        <img src="'.asset('images/icons/icon-programas-5.svg').'"
                                                            align="middle" class="w-100 estandar-iconos ">
                                                    </div>
                                                    <div class="col-10 align-self-center">
                                                        <p
                                                            class="mb-0 text-white align-self-center text-blue contenido-popups ">
                                                            <b>Ejercicio aeróbico</b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>'
            ]
            )
            <!-- Planificación mensual -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'modalID' => 'pm4',
            'titulo' => 'Programa 4',
            'customDialog' => 'especialdesk',
            'footer' => 1,
            'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="4" data-mes="1">
                        <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-4-1"><b>Mes 1</b></p>
                    </div>
                    <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="4" data-mes="2">
                        <p class="mb-3 text-blue contenido-popups js-mesactivo-4-2"><b>Mes 2</b></p>
                    </div>
                    <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="4" data-mes="3">
                        <p class="mb-3 text-blue contenido-popups js-mesactivo-4-3"><b>Mes 3</b></p>
                    </div>
                </div>
            </div>
            <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
                <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
                <p class="mb-0 contenido-popups text-white">Mar</p>
                <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
                <p class="mb-0 contenido-popups text-white">Jue</p>
                <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
                <p class="mb-0 contenido-popups text-white">Sáb</p>
                <p class="mb-0 contenido-popups text-white">Dom</p>
            </div>
            <div class="js-mes-4-1">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1 ">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
                <div class="container-fluid mb-4">
                    <div class="row">
                        <div class="col-5 explicacion py-2">
                            <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                            <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                            <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                            <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                        </div>
                        <div class="col-7">
                            <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe
                                    realizar en este día.</b></p>
                            <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que
                                    se realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los
                                    segundos sobre cada pie.</b></p>
                            <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso
                                    entre cada ejercicio.</b></p>
                            <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que
                                    debemos realizar el circuito de ejercicios completo.</b></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="js-mes-4-2" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
            </div>
            <div class="js-mes-4-3" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1 ">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones
                                </p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                    <div class="row p-0">
                        <div class="col-4">
                            <div class="color-green d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-yellow d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="color-coral d-block px-2 py-1">
                                <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="w-100 text-right mt-2">
                    <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
            </div>'
            ]
            )
            <!-- fin Planificación mensual -->

            <!-- Circuitos de ejercicios -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce4',
            'titulo' => 'Programa 4',
            'footer' => 1,
            'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
            <div class="container-fluid p-0">
                <div class="row p-0">
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa4/ejercicio1.png').'" align="middle"
                            class="w-100 mb-2">
                        <button class="color-blue newbuttons w-100 mb-3 py-3 border-0 text-white cursor-pointer"
                            type="button" data-toggle="collapse" data-target="#circuito41" aria-expanded="false"
                            aria-controls="circuito41">Ejercicios Circuito 1</button>
                        <div class="collapse multi-collapse" id="circuito41">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce411"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce412"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce413"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce414"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce415"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce416"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce417"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa4/ejercicio2.png').'" align="middle"
                            class="w-100 mb-2">
                        <button class="color-blue newbuttons w-100 mb-3 py-3 border-0 text-white" type="button"
                            data-toggle="collapse" data-target="#circuito32" aria-expanded="false"
                            aria-controls="circuito32">Ejercicios Circuito 2</button>
                        <div class="collapse multi-collapse" id="circuito32">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce421"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce422"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce423"><b>Hombro</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce424"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce425"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce426"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce427"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa4/ejercicio3.png').'" align="middle"
                            class="w-100  mb-2">
                        <button class="color-blue newbuttons w-100 mb-3 py-3 border-0 text-white" type="button"
                            data-toggle="collapse" data-target="#circuito33" aria-expanded="false"
                            aria-controls="circuito33">Ejercicios Circuito 3</button>
                        <div class="collapse multi-collapse" id="circuito33">
                            <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce431"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce432"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce433"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce434"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce435"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce436"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal"
                                    data-target="#ce437"><b>Espalda</b></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            '
            ]
            )
            <!-- Circuito 1 -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce411',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705206" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce412',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705224" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce413',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705243" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce414',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705265" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce415',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705285" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce416',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705307" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce417',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705328" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 1 -->

            <!-- Circuito 2 -->

            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce421',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705344" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce422',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705360" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce423',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705380" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce424',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705393" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce425',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705409" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce426',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705418" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce427',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705430" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 2 -->

            <!-- Circuito 3 -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce431',
            'titulo' => 'Sentadillas',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705455" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce432',
            'titulo' => 'Equilibrio',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705474" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce433',
            'titulo' => 'Pecho',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705489" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce434',
            'titulo' => 'Tronco inferior',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705521" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce435',
            'titulo' => 'Glúteo',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705535" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce436',
            'titulo' => 'Abdomen',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705549" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'customDialog' => 'especialdesk',
            'modalID' => 'ce437',
            'titulo' => 'Espalda',
            'footer' => 1,
            'contenido' => ' <div class="embed-responsive embed-responsive-1by1">
                <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705571" frameborder="0"
                    scrolling="no" allowfullscreen></iframe>
            </div>'
            ]
            )
            <!-- FIN Circuito 3 -->

            <!-- fin Circuitos de ejercicios -->

            <!-- Ejercicios aerobicos -->
            @include('modals.simple-modal-principal',
            [
            'customClass' => 'bg-white',
            'modalID' => 'ap4',
            'titulo' => 'Programa 4',
            'customDialog' => 'especialdesk',
            'footer' => 1,
            'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
            <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
                <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
            </div>
            <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000
                    pasos</b></p>
            <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b>
                        </p>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3 p-0">
                        <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                            <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                        </div>
                    </div>
                    <div class="col-9">
                        <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales
                            <b>al menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b>
                        </p>
                    </div>
                </div>
            </div>'
            ]
            )
            <!-- Fin Ejercicios aerobicos -->
        @endif
    @endif

    <!-- POPUP Coral -->
    @include('modals.simple-modal-principal',
    [
    'customClass' => 'bg-white',
    'modalID' => 'cronograma',
    'titulo' => 'Cronograma general del estudio',
    'footer' => 1,
    'customDialog' => 'especialdesk',
    'contenido' => '
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 col-md-5">
                <img src="'.asset('images/cronograma/img_cronograma.png').'" align="middle" class="img-fluid">
            </div>
            <div class="col-12 col-md-7 align-self-center">
                <ul class="pl-4">
                    <li class="text-blue contenido-popups mb-1"><b>Valoración integral:</b> función física, fragilidad,
                        estado nutricional, composición corporal mediante ecografía muscular y bioimpedancia y parámetros de
                        calidad de vida.</li>
                    <li class="text-blue contenido-popups mb-1"><b>Muestras de sangre y heces:</b> para el estudio del
                        impacto del ejercicio en los marcadores de inflamación y par a el estudio de edad biológica.</li>
                    <li class="text-blue contenido-popups mb-1"><b>Reevaluación:</b> del programa de ejercicio
                        multicomponente personalizado.</li>
                </ul>
            </div>
        </div>
    </div> '
    ]
    )

@endsection
