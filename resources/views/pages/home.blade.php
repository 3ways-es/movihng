@extends('layouts.app')
@section('content')
<div class="fondo">
   <div class="container-fluid">
      <div class="row bloquetexto py-3 py-md-5 justify-content-center align-items-center"">
         <div class="col-12 contenedor-global text-center">
            <img src="{{ asset('images/logo.png') }}" align="middle" class="logo-principal" id="logoprincipal">
               <p class="mb-0 text-white text-justify overflow-texto" style="display:none;" id="bloque-texto">Como destaca el “Informe Mundial Sobre el
Envejecimiento y la Salud” de la Organización
Mundial de la Salud (OMS) de 2015
<a  class="text-grey" href="http://apps.who.int/iris/bitstream/10665/18
6466/1/9789240694873_spa.pdf?ua=1" target="_blank">
http://apps.who.int/iris/bitstream/10665/18
6466/1/9789240694873_spa.pdf?ua=1.</a>, el
<b class="text-green">envejecimiento saludable</b> no se define por la
ausencia de enfermedad sino por la capacidad
funcional que permite a la persona mayor
relacionarse con el entorno y ser autónoma.
El objetivo es alcanzar edades avanzadas con
buena calidad de vida, con independencia y
sin discapacidad. La fragilidad y la función
física son mejores marcadores pronósticos de
morbimortalidad en las personas mayores
que la presencia de comorbilidades. Lo más
importante es que la fragilidad se puede
revertir y el deterioro de la función física se
puede mejorar si se detectan de forma precoz
y se aplican las medidas adecuadas <b class="text-green">siendo la
fundamental el ejercicio físico</b>. Además del
beneficio físico, el ejercicio tiene un impacto positivo en la salud cardiopulmonar, cognitiva
y afectiva, mejorando la capacidad para
establecer relaciones sociales y
disminuyendo el aislamiento.
A pesar de la evidencia sólida del beneficio
que el ejercicio físico aporta a la salud de la
personas mayores, de la incipiente evidencia
específicamente en la población mayor con
VIH, y al reto demográfico al que nos
enfrentamos, no estamos asistiendo a la
promoción de medidas concretas en las
políticas de salud de los mayores con VIH que
vayan en la línea marcada por la OMS por lo
que creemos necesario realizar el estudio que
proponemos ya que puede arrojar datos de
gran relevancia para la promoción del
envejecimiento saludable en la población
mayor con VIH.
Recomendaciones de la OMS de ejercicio físico
<a  class="text-grey" href="https://apps.who.int/iris/bitstream/handle/1
0665/337004/9789240014817-spa.pdf" target="_blank">
https://apps.who.int/iris/bitstream/handle/1
0665/337004/9789240014817-spa.pdf</a></p>
         </div>
      </div>
   </div>
   <div class="navbar fixed-bottom navbar-dark d-block mb-3">
         <div class="d-flex justify-content-center mb-3">
            <div class="circulo activo principal" data-tipo="principal"></div>
            <div class="circulo noactivo ml-4 secundaria" data-tipo="secundaria"></div>
         </div>
         <div class="container-fluid d-block">
            <div class="row">
               <div class="col-6 text-center">
                  <div class="d-none d-sm-none d-md-block">
                     <button type="button" class="newbuttons color-yellow text-blue w-100 py-3 d-block text-decoration-none border-0" data-toggle="modal" data-target="#objetivos">Objetivos del estudio</button>
                     </div>
                  <div class="d-block d-sm-block d-md-none">
                     <a class="newbuttons color-yellow text-blue w-100 py-2 d-block text-decoration-none" href="{{ url('/objetivos') }}">Objetivos<br>del estudio</a>
                  </div>
               </div>
               <div class="col-6 text-center">
                  <div class="d-none d-sm-none d-md-block">
                     <button type="button" class="newbuttons color-coral text-blue w-100 py-3 d-block text-decoration-none border-0" data-toggle="modal" data-target="#contacta">Contacta con nosotros</button>
                     </div>
                  <div class="d-block d-sm-block d-md-none">
                  <a class="newbuttons color-coral text-blue w-100 py-2 d-block text-decoration-none" href="{{ url('/contacta') }}">Contacta<br>con nosotros</a>
                  </div>
               </div>
            </div>
            <div class="row mt-4 mb-2">
               <div class="col-12 text-center">
                  <a class="newbuttons color-green text-white w-100 py-3 d-block text-decoration-none" href="{{ url('/home') }}">Acceso Participantes</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@stop
@section('modals')

@include('modals.simple-modal', ['customClass' => 'color-yellow', 'modalID' => 'objetivos', 'titulo' => '<p class="text-blue titulo mb-0">Objetivos<br>del estudio</p>', 'contenido' => '<p class="text-blue text-justify">El objetivo principal del estudio MOVIhNG es analizar el efecto de un programa de ejercicio multicomponente personalizado (PEMP) en condiciones de vida real en la composición corporal, la función física, la fragilidad y la calidad de vida de las personas mayores con VIH y compararlo con el efecto en personas mayores de la misma edad VIH negativos.</p>
   <p class="text-blue mb-0 text-justify">Son también objetivos del estudio MOVIhNG:</p>
             <ul class="text-blue text-justify pl-4">
                <li>Analizar los efectos de un PEMP en
condiciones de vida real en la edad biológica, los biomarcadores de función muscular, en los biomarcadores inmunológicos y en el microbioma.</li>
                <li>Evaluar la adherencia a un PEMP en condiciones de vida real con supervisión estrecha, con supervisión media y sin supervisión.
             </li> 
          </ul>'
          ])

@include('modals.simple-modal', ['customClass' => 'color-coral', 'modalID' => 'contacta', 'titulo' => '<p class="text-blue titulo mb-0 font-weight-bold">Contacta<br>con nosotros</p>', 'contenido' => '<a class="btn color-blue text-white w-100" href="mailto:movihng@gmail.com">movihng@gmail.com</a>'
          ])
          @endsection
