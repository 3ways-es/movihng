@extends('layouts.app', ['home' => 1])
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/principal.css') }}?x={{ rand(5, 15) }}">
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/navbar.js') }}?x={{ rand(5, 15) }}"></script>
@endsection
@section('content')
<div class="container w-75 last-item">
   <div class="row">
      <div class="col-12 mt-4 mb-2 p-0">
        <h2 class="text-blue">Calendario de citas</h2>
      </div>
   </div>
   @if (count($citas) > 0)
   <div class="row">
      <div class="col-12 p-0">
          @foreach ($citas as $indexmeses => $meses)
            <div class="color-blue newbuttons py-3 px-3 text-left mt-2 mb-3">
                  @php
                     switch ($indexmeses) {
                        case 'January':
                           $mesEs = 'Enero';
                           break;
                        case 'February':
                           $mesEs = 'Febrero';
                           break;
                        case 'March':
                           $mesEs = 'Marzo';
                           break;
                        case 'April':
                           $mesEs = 'Abril';
                           break;
                        case 'May':
                           $mesEs = 'Mayo';
                           break;
                        case 'June':
                           $mesEs = 'Junio';
                           break;
                        case 'July':
                           $mesEs = 'Julio';
                           break;
                        case 'August':
                           $mesEs = 'Agosto';
                           break;
                        case 'September':
                           $mesEs = 'Septiembre';
                           break;
                        case 'October':
                           $mesEs = 'Octubre';
                           break;
                        case 'November':
                           $mesEs = 'Noviembre';
                           break;
                        case 'December':
                           $mesEs = 'Diciembre';
                           break;
                     }
                  @endphp
                  <p class="mb-0 titulo-popups text-white"><b>{{ $mesEs }}</b></p>
            </div>
            <div class="container-fluid">
            @foreach ($meses as $index => $value)
               @php
                     switch (date('l', strtotime($value->fecha_cita))) {
                        case 'Monday':
                           $semanaEs = 'Lunes';
                           break;
                        case 'Tuesday':
                           $semanaEs = 'Martes';
                           break;
                        case 'Wednesday':
                           $semanaEs = 'Miércoles';
                           break;
                        case 'Thursday':
                           $semanaEs = 'Jueves';
                           break;
                        case 'Friday':
                           $semanaEs = 'Viernes';
                           break;
                         case 'Saturday':
                           $semanaEs = 'Sábado';
                           break;
                        case 'Sunday':
                           $semanaEs = 'Domingo';
                           break;
                     }
                  @endphp
               <div class="row">
                  <div class="col-3 pl-1 separador-citas">
                        <p class="mb-0 titulo-popups fechas-calendario-citas text-blue"><b>{{ date('d', strtotime($value->fecha_cita)) }}</b></p>
                        <p class="mb-0 contenido-popups text-blue"><b>{{ substr($semanaEs, 0, 3) }}</b></p>
                        <p class="mb-0 contenido-popups text-blue"><b>{{ date('H:i', strtotime($value->fecha_cita)) }}</b></p>
                  </div>
                  <div class="col-9">
                     <div class="color-{{ $value->color }} px-2 py-4 mb-3">
                        <p class="mb-0 titulo-popups text-blue"><b>{{ $value->titulo }}</b></p>
                        <p class="mb-0 contenido-popups text-white"><b>{{ $value->descripcion }}</b></p>
                     </div>
                  </div>
               </div>
            @endforeach
            </div>
         @endforeach
      </div>
   </div>
   @else
   <div class="row">
      <div class="col-12 p-0">
         <h2 class="text-blue">No tiene citas</h2>
      </div>
   </div>
   @endif
</div>
@stop
