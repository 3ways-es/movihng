@extends('layouts.app', ['home' => 1])
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/perfil.css') }}?x={{ rand(5, 15) }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/principal.css') }}?x={{ rand(5, 15) }}">
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/navbar.js') }}?x={{ rand(5, 15) }}"></script>
@endsection
@section('content')
@if (@$_GET['b']==1)
   <div class="alert alert-success alert-dismissible fade show" role="alert">
     <strong>Diario de Actividad Guardado</strong>
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
     </button>
   </div>
@elseif(@$_GET['b']==3)
<div class="alert alert-warning alert-dismissible fade show" role="alert">
     <strong>Ya has completado todos los diarios de actividad</strong>
     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
     </button>
   </div>
@endif
<div class="container w-75">
   <div class="row pt-2">
         <div class="col-12 text-left p-0">
            <a class="text-blue" href="{{ route('principal') }}"><i class="fas fa-chevron-left"></i></a>
         </div>
   </div>
   <div class="row">
      <div class="col-12 my-4 p-0">
        <h2 class="text-blue">Zona personal privada</h2>
      </div>
   </div>
   <div class="row mt-3">
      <a href="{{ route('perfil.calendario-de-citas') }}"  style="text-decoration: none" class="w-100">
         <div class="col-12 color-yellow py-5 px-3 newbuttons container-fluid botones-principal">
               <div class="row">
                  <div class="col-2 pr-0">
                      <img src="{{ asset('images/icons/icon-programas-1.svg') }}" align="middle" class="w-100 estandar-iconos">
                  </div>
                  <div class="col-10 align-self-center">
                     <p class="mb-0 text-blue align-self-center text-bloques"><b>Calendario de citas</b></p>
                  </div>
              </div>
         </div>
      </a>
   </div>
   <div class="row mt-3">
      <div class="col-12 color-green py-5 px-3 newbuttons container-fluid botones-principal" data-toggle="modal" data-target="#actividad">
         <div class="row">
            <div class="col-2 pr-0">
                <img src="{{ asset('images/icons/actividad.svg') }}" align="middle" class="w-100 estandar-iconos">
            </div>
            <div class="col-10 align-self-center">
               <p class="mb-0 text-white align-self-center text-bloques"><b>Diario de actividad</b></p>
            </div>
        </div>
      </div>
   </div>
   <div class="row mt-3 bloque-final">
      <div class="col-12 color-coral py-5 px-3 newbuttons container-fluid botones-principal" data-toggle="modal" data-target="#contacta">
         <div class="row">
            <div class="col-2 pr-0">
               <img src="{{ asset('images/icons/contacto.svg') }}" align="middle" class="w-100 estandar-iconos">
            </div>
            <div class="col-10 align-self-center">
               <p class="mb-0 text-blue align-self-center text-bloques"><b>Contacto</b></p>
            </div>
         </div>
      </div>
   </div>
</div>
@stop
@section('modals')
@if (\Auth::user()->programa_id == 1)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'actividad', 
   'titulo' => 'Diario de la actividad',
   'footer' => 1, 
   'customDialog' => 'especialdesk',
   'contenido' => '
   <div class="row justify-content-center">
      <div class="col-12 col-md-6">
         <a href='.route('perfil.diario-de-la-actividad', ['programa' => 1]).'>
            <img src="'.asset('images/diariodeactividad/programa1.png').'" align="middle" class="img-fluid">
         </a>
      </div>
   </div>'       
   ]
)
@elseif(\Auth::user()->programa_id == 2)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'actividad',
   'footer' => 1, 
   'customDialog' => 'especialdesk',
   'titulo' => 'Diario de la actividad',
   'contenido' => '
   <div class="row justify-content-center">
      <div class="col-12 col-md-6">
         <a href='.route('perfil.diario-de-la-actividad', ['programa' => 2]).'>
            <img src="'.asset('images/diariodeactividad/programa2.png').'" align="middle" class="img-fluid">
         </a> 
      </div>
   </div>'       
   ]
)
@elseif(\Auth::user()->programa_id == 3)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'actividad',
   'footer' => 1, 
   'customDialog' => 'especialdesk',
   'titulo' => 'Diario de la actividad',
   'contenido' => '
   <div class="row justify-content-center">
      <div class="col-12 col-md-6">
         <a href='.route('perfil.diario-de-la-actividad', ['programa' => 3]).'>
            <img src="'.asset('images/diariodeactividad/programa3.png').'" align="middle" class="img-fluid">
         </a> 
      </div>
   </div>'       
   ]
)
@else
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'actividad',
   'footer' => 1, 
   'customDialog' => 'especialdesk',
   'titulo' => 'Diario de la actividad',
   'contenido' => '
   <div class="row justify-content-center">
      <div class="col-12 col-md-6">
         <a href='.route('perfil.diario-de-la-actividad', ['programa' => 4]).'>
            <img src="'.asset('images/diariodeactividad/programa4.png').'" align="middle" class="img-fluid">
         </a> 
      </div>
   </div>'       
   ]
)
@endif

@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'contacta', 
   'titulo' => 'Contacta',
   'footer' => 1, 
   'customDialog' => 'especialdesk',
   'contenido' => '
   <div class="container-fluid">
      <div class="row">
         <div class="col-12 col-md-6 p-0 pr-md-1">
            <p class="mb-0 text-blue contenido-popups">El entrenador personal se pondrá en contacto contigo una vez a la semana para ver cómo vas con la realización del programa de entrenamiento. Como respuesta a su mensaje puedes comentarle si tienes alguna duda o dificultad con el progr ama.</p>
            <a href="'.route('chat').'" style="text-decoration: none" class="w-100">
               <div class="color-yellow mt-2 py-4 px-2 newbuttons container-fluid botones-principal"">
                  <div class="row">
                     <div class="col-2 pr-0">
                         <img src="'.asset('images/icons/icon-entrenador.svg') .'" align="middle" class="w-100 estandar-iconos">
                     </div>
                     <div class="col-10 align-self-center">
                        <p class="mb-0 text-blue align-self-center text-bloques"><b>Entrenador personal</b></p>
                     </div>
                 </div>
               </div>
            </a>
         </div>
         <div class="col-12 col-md-6 mt-4 mt-md-0 p-0 pl-md-1">
            <p class="mb-0 text-blue contenido-popups">Si en otro momento de la semana tienes dudas con respecto al programa de entrenamiento o cualquier otra cuestión ponte en contacto con el equipo investigador a través del correo <a class="text-blue font-weight-bold w-100" href="mailto:movihng@gmail.com">movihng@gmail.com</a></p>
            <a href="mailto:movihng@gmail.com"  style="text-decoration: none" class="w-100">
               <div class="color-green mt-2 py-4 px-2 newbuttons container-fluid botones-principal"">
                  <div class="row">
                     <div class="col-2 pr-0">
                         <img src="'.asset('images/icons/icon-contacta.svg') .'" align="middle" class="w-100 estandar-iconos">
                     </div>
                     <div class="col-10 align-self-center">
                        <p class="mb-0 text-white align-self-center text-bloques"><b>Contactar con el equipo</b></p>
                     </div>
                 </div>
               </div>
            </a>
         </div>
      </div>
   </div>
   '       
   ]
)

@endsection