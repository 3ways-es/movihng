@extends('layouts.app', ['home' => 1])
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/principal.css') }}?x={{ rand(5, 15) }}">
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/navbar.js') }}?x={{ rand(5, 15) }}"></script>
@endsection
@section('content')
<div class="container w-75 last-item">
   <div class="row">
      <div class="col-12 mt-4 mb-2 p-0">
        <h2 class="text-blue">Contacto</h2>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-12 col-md-6 p-0">
            <p class="mb-0 text-blue contenido-popups">El entrenador personal se pondrá en contacto contigo una vez a la semana para ver cómo vas con la realización del programa de entrenamiento. Como respuesta a su mensaje puedes comentarle si tienes alguna duda o dificultad con el progr ama.</p>
            <a href=""  style="text-decoration: none" class="w-100">
               <div class="color-yellow mt-2 py-4 px-2 newbuttons container-fluid botones-principal"">
                  <div class="row">
                     <div class="col-2 pr-0">
                         <img src="{{ asset('images/icons/icon-entrenador.svg') }}" align="middle" class="w-100 estandar-iconos">
                     </div>
                     <div class="col-10 align-self-center">
                        <p class="mb-0 text-blue align-self-center text-bloques"><b>Entrenador personal</b></p>
                     </div>
                 </div>
               </div>
            </a>
         </div>
         <div class="col-12 col-md-6 mt-4 mt-md-0 p-0">
            <p class="mb-0 text-blue contenido-popups">Si en otro momento de la semana tienes dudas con respecto al programa de entrenamiento o cualquier otra cuestión ponte en contacto con el equipo investigador a través del correo <a class="text-blue font-weight-bold w-100" href="mailto:movihng@gmail.com">movihng@gmail.com</a></p>
            <a href="mailto:movihng@gmail.com"  style="text-decoration: none" class="w-100">
               <div class="color-green mt-2 py-4 px-2 newbuttons container-fluid botones-principal"">
                  <div class="row">
                     <div class="col-2 pr-0">
                         <img src="{{ asset('images/icons/icon-contacta.svg') }}" align="middle" class="w-100 estandar-iconos">
                     </div>
                     <div class="col-10 align-self-center">
                        <p class="mb-0 text-white align-self-center text-bloques"><b>Contactar con el equipo</b></p>
                     </div>
                 </div>
               </div>
            </a>
         </div>
      </div>
   </div>
</div>
@stop
