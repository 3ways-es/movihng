@extends('layouts.app', ['home' => 1])
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/perfil.css') }}?x={{ rand(5, 15) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/principal.css') }}?x={{ rand(5, 15) }}">
@endsection
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/perfil.js') }}?x={{ rand(5, 15) }}"></script>
@endsection
@section('content')
    <div class="container-fluid bloque-final">
        <div class="row pt-5">
            <div class="col-12 text-left">
                <a class="text-blue" href="{{ route('perfil') }}"><i class="fas fa-chevron-left"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-12 my-4">
                <h2 class="text-blue">Diario de la actividad<br>Programa {{ $programa }}</h2>
            </div>
        </div>
        <div class="row js-meses">
            <div class="col-12 ">
                <div class="color-blue newbuttons py-3 px-3 text-left mt-2 mb-3">
                    <select class="color-blue text-white border-0 w-100" id="messelecionado"
                        data-programa="{{ $programa }}">
                        @if (@$meses)
                            @foreach ($meses as $index => $mes)
                                <option id="mes-{{ $index + 1 }}" value="{{ $index + 1 }}"
                                    {{ !$mes['valido'] ? 'disabled' : '' }}>Mes {{ $index + 1 }}
                                    {{ !$mes['valido'] ? 'Enviado' : '' }}</option>
                            @endforeach
                        @else
                            <option id="mes-1" value="1" selected>Mes 1</option>
                            @for ($i = 2; $i <= 12; $i++)
                            <option id="mes-{{$i}}" value="{{$i}}">Mes {{$i}}</option>
                            @endfor
                        @endif
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 ">
                <div class="color-blue newbuttons py-3 px-3 text-left mt-2 mb-3">
                    <select class="color-blue text-white border-0 w-100" id="semanaseleccionada">
                        <option id="semana-1" value="1" {{ $semana[1] == 1 ? 'disabled' : '' }}>Semana 1</option>
                        <option id="semana-2" value="2" {{ $semana[2] == 1 ? 'disabled' : '' }}>Semana 2</option>
                        <option id="semana-3" value="3" {{ $semana[3] == 1 ? 'disabled' : '' }}>Semana 3</option>
                        <option id="semana-4" value="4" {{ $semana[4] == 1 ? 'disabled' : '' }}>Semana 4</option>
                    </select>
                </div>
            </div>
        </div>
        <form action="/crearactividad" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" class="hidden" value="{{ $programa }}" id="programa" name="programa">
            @php
                if ($semana[1] == 0) {
                    $semana = 1;
                } elseif ($semana[2] == 0) {
                    $semana = 2;
                } elseif ($semana[3] == 0) {
                    $semana = 3;
                } elseif ($semana[4] == 0) {
                    $semana = 4;
                }
            @endphp
            <input type="hidden" class="hidden" value="{{ $semana }}" id="semana" name="semana">
            <input type="hidden" class="hidden" value="{{ $primermes }}" id="mes" name="mes">
            <!-- Pasos -->
            <div class="row">
                <div class="col-12 ">
                    <p class="mb-0 titulos-encuesta text-blue"><b>Pasos</b></p>
                    <p class="mb-0 contenido-popups text-blue"><b>Lo puedes ver al final de cada día en tu pulsera de
                            actividad</b></p>
                </div>
            </div>
            <div class="row mt-1">
                <div class="col-4 ">
                    <div class="form-group mb-0">
                        <label for="plunes" class="contenido-popups text-blue">Lun</label>
                        <input type="number" class="form-control color-grisclaro text-left py-2 input-pasos" id="plunes"
                            placeholder="0" name="plunes" required value="{{ @old('plunes') ? old('plunes') : 0 }}">
                        @if ($errors->has('plunes'))
                            <div class="alert alert-danger">
                                {{ $errors->first('plunes') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-4 ">
                    <div class="form-group mb-0">
                        <label for="pmartes" class="contenido-popups text-blue">Mar</label>
                        <input type="number" class="form-control color-grisclaro text-left py-2 input-pasos" id="pmartes"
                            placeholder="0" name="pmartes" required value="{{ @old('pmartes') ? old('pmartes') : 0 }}">
                        @if ($errors->has('pmartes'))
                            <div class="alert alert-danger">
                                {{ $errors->first('pmartes') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-4 ">
                    <div class="form-group mb-0">
                        <label for="pmiercoles" class="contenido-popups text-blue">Mié</label>
                        <input type="number" class="form-control color-grisclaro  text-left py-2 input-pasos"
                            id="pmiercoles" placeholder="0" name="pmiercoles" required
                            value="{{ @old('pmiercoles') ? old('pmiercoles') : 0 }}">
                        @if ($errors->has('pmiercoles'))
                            <div class="alert alert-danger">
                                {{ $errors->first('pmiercoles') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-4 mt-2">
                    <div class="form-group mb-0">
                        <label for="pjueves" class="contenido-popups text-blue">Jue</label>
                        <input type="number" class="form-control color-grisclaro text-left py-2 input-pasos" id="pjueves"
                            placeholder="0" name="pjueves" required value="{{ @old('pjueves') ? old('pjueves') : 0 }}">
                        @if ($errors->has('pjueves'))
                            <div class="alert alert-danger">
                                {{ $errors->first('pjueves') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-4 mt-2">
                    <div class="form-group mb-0">
                        <label for="pviernes" class="contenido-popups text-blue">Vie</label>
                        <input type="number" class="form-control color-grisclaro text-left py-2 input-pasos" id="pviernes"
                            placeholder="0" name="pviernes" required
                            value="{{ @old('pviernes') ? old('pviernes') : 0 }}">
                        @if ($errors->has('pviernes'))
                            <div class="alert alert-danger">
                                {{ $errors->first('pviernes') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-4 mt-2">
                    <div class="form-group mb-0">
                        <label for="psabado" class="contenido-popups text-blue">Sab</label>
                        <input type="number" class="form-control color-grisclaro text-left py-2 input-pasos" id="psabado"
                            placeholder="0" name="psabado" required value="{{ @old('psabado') ? old('psabado') : 0 }}">
                        @if ($errors->has('psabado'))
                            <div class="alert alert-danger">
                                {{ $errors->first('psabado') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-4 mt-2">
                    <div class="form-group mb-0">
                        <label for="pdomingo" class="contenido-popups text-blue">Dom</label>
                        <input type="number" class="form-control color-grisclaro text-left py-2 input-pasos" id="pdomingo"
                            placeholder="0" name="pdomingo" required
                            value="{{ @old('pdomingo') ? old('pdomingo') : 0 }}">
                        @if ($errors->has('pdomingo'))
                            <div class="alert alert-danger">
                                {{ $errors->first('pdomingo') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-8 mt-2">
                    <div class="form-group mb-0">
                        <label for="pmedia" class="contenido-popups text-blue">Media Semanal</label>
                        @php
                            $total = @old('plunes') ? old('plunes') : 0;
                            $total += @old('pmartes') ? old('pmartes') : 0;
                            $total += @old('pmiercoles') ? old('pmiercoles') : 0;
                            $total += @old('pjueves') ? old('pjueves') : 0;
                            $total += @old('pviernes') ? old('pviernes') : 0;
                            $total += @old('psabado') ? old('psabado') : 0;
                            $total += @old('pdomingo') ? old('pdomingo') : 0;
                            
                            if ($total > 0) {
                                $media = $total / 7;
                            }
                        @endphp
                        <input type="number" class="form-control color-grey  text-left py-2" id="pmedia" placeholder=""
                            name="pmedia" readonly value="{{ @$media ? round($media) : '' }}">
                    </div>
                </div>
            </div>
            <!-- FIN Pasos -->
            <!-- Frecencia Media semanal -->
            <div class="row mt-4">
                <div class="col-12">
                    <p class="mb-0 titulos-encuesta text-blue"><b>Frecuencia cardiaca media semanal</b></p>
                    <div class="form-group mb-0">
                        <label for="frecuenciamediasemanal" class="d-none"></label>
                        <input type="number" class="form-control color-grisclaro text-left py-2 mt-1"
                            id="frecuenciamediasemanal" placeholder="0" name="frecuenciamediasemanal"
                            value="{{ @old('frecuenciamediasemanal') ? old('frecuenciamediasemanal') : 0 }}">
                        @if ($errors->has('frecuenciamediasemanal'))
                            <div class="alert alert-danger">
                                {{ $errors->first('frecuenciamediasemanal') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- FIN Frecencia Media semanal -->
            <!-- Frecencia máxima semanal -->
            <div class="row mt-4">
                <div class="col-12">
                    <p class="mb-0 titulos-encuesta text-blue"><b>Frecuencia cardiaca máxima en los tramos rápidos</b></p>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-6 pl-0">
                                <div class="form-group mb-0">
                                    <label for="frecuenciamaxima1" class="d-none"></label>
                                    <input type="number" class="form-control color-grisclaro text-left py-2 mt-1"
                                        id="frecuenciamaxima1" placeholder="0" name="frecuenciamaxima1"
                                        value="{{ @old('frecuenciamaxima1') ? old('frecuenciamaxima1') : 0 }}">
                                    @if ($errors->has('frecuenciamaxima1'))
                                        <div class="alert alert-danger">
                                            {{ $errors->first('frecuenciamaxima1') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6 pr-0">
                                <div class="form-group mb-0">
                                    <label for="frecuenciamaxima2" class="d-none"></label>
                                    <input type="number" class="form-control color-grisclaro text-left py-2 mt-1"
                                        id="frecuenciamaxima2" placeholder="0" name="frecuenciamaxima2"
                                        value="{{ @old('frecuenciamaxima2') ? old('frecuenciamaxima2') : 0 }}">
                                    @if ($errors->has('frecuenciamaxima2'))
                                        <div class="alert alert-danger">
                                            {{ $errors->first('frecuenciamaxima2') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN Frecencia máxima semanal -->
            <!-- Adherencia -->
            <div class="row mt-4">
                <div class="col-12">
                    <p class="mb-0 titulos-encuesta text-blue"><b>Adherencia al ejercicio</b></p>
                    <p class="mb-0 contenido-popups text-blue">Si esta semana has hecho todos los ejercicios del programa
                        señala el cuadrado verde</p>
                    <p class="mb-0 contenido-popups text-blue">Si esta semana has hecho la mitad o más señala el cuadrado
                        amarillo</p>
                    <p class="mb-0 contenido-popups text-blue">Si esta semana has hecho menos de la mitad señala el cuadrado
                        rojo</p>
                    <div class="form-group mb-0 row mt-1">
                        <div class="col-4 text-center">
                            <label for="adherencia1" class="label-img w-100">
                                <input type="radio" id="adherencia1" name="adherencia" value="1" class="d-none">
                                <img src="{{ asset('images/icons/cara1.svg') }}" class="w-100 btn-adhesion color-green">
                            </label>
                        </div>
                        <div class="col-4 text-center">
                            <label for="adherencia2" class="label-img w-100">
                                <input type="radio" id="adherencia2" name="adherencia" value="2" class="d-none">
                                <img src="{{ asset('images/icons/cara2.svg') }}" class="w-100 btn-adhesion color-yellow">
                            </label>
                        </div>
                        <div class="col-4 text-center">
                            <label for="adherencia3" class="label-img w-100">
                                <input type="radio" id="adherencia3" name="adherencia" value="3" class="d-none">
                                <img src="{{ asset('images/icons/cara3.svg') }}" class="w-100 btn-adhesion color-coral">
                            </label>
                        </div>
                        @if ($errors->has('adherencia'))
                            <div class="col-12 text-center">
                                <div class="alert alert-danger">
                                    {{ $errors->first('adherencia') }}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- FIN Adherencia -->
            <!-- Esfuerzo -->
            <div class="row mt-4">
                <div class="col-12">
                    <p class="mb-0 titulos-encuesta text-blue"><b>¿Cuánto esfuerzo te ha supuesto el ejercicio de esta
                            semana?</b></p>
                    <p class="mb-0 contenido-popups text-blue">1 es nada de esfuerzo 10 es un esfuerzo máximo</p>
                    <div class="form-group mb-0 d-block mt-1 text-center w-100">
                        <label for="esfuerzo1">
                            <input type="radio" id="esfuerzo1" name="esfuerzo" value="1" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">1</p>
                            </div>
                        </label>
                        <label for="esfuerzo2">
                            <input type="radio" id="esfuerzo2" name="esfuerzo" value="2" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">2</p>
                            </div>
                        </label>
                        <label for="esfuerzo3">
                            <input type="radio" id="esfuerzo3" name="esfuerzo" value="3" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">3</p>
                            </div>
                        </label>
                        <label for="esfuerzo4">
                            <input type="radio" id="esfuerzo4" name="esfuerzo" value="4" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">4</p>
                            </div>
                        </label>
                        <label for="esfuerzo5">
                            <input type="radio" id="esfuerzo5" name="esfuerzo" value="5" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">5</p>
                            </div>
                        </label>
                        <label for="esfuerzo6">
                            <input type="radio" id="esfuerzo6" name="esfuerzo" value="6" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">6</p>
                            </div>
                        </label>
                        <label for="esfuerzo7">
                            <input type="radio" id="esfuerzo7" name="esfuerzo" value="7" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">7</p>
                            </div>
                        </label>
                        <label for="esfuerzo8">
                            <input type="radio" id="esfuerzo8" name="esfuerzo" value="8" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">8</p>
                            </div>
                        </label>
                        <label for="esfuerzo9">
                            <input type="radio" id="esfuerzo9" name="esfuerzo" value="9" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">9</p>
                            </div>
                        </label>
                        <label for="esfuerzo10">
                            <input type="radio" id="esfuerzo10" name="esfuerzo" value="10" class="d-none">
                            <div class="py-2 px-3 color-grisclaro btn-esfuerzo">
                                <p class="mb-0 contenido-popups text-blue">10</p>
                            </div>
                        </label>
                        @if ($errors->has('esfuerzo'))
                            <div class="alert alert-danger">
                                {{ $errors->first('esfuerzo') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- FIN Esfuerzo -->
            <!-- Calidad salud -->
            <div class="row mt-4">
                <div class="col-12">
                    <p class="mb-0 titulos-encuesta text-blue"><b>¿Cómo crees que ha sido la calidad de tu salud esta
                            semana?</b></p>
                    <p class="mb-0 contenido-popups text-blue">Señala en esta escala como crees que ha sido tu estado de
                        salud esta semana: el 100 es el mejor estado de salud posible y el 0 el peor posible.</p>
                    <label for="calidadsalud" class="d-none"></label>
                    <div class="row">
                        <div class="col-2 align-self-center">
                            <p class="mb-0 contenido-popups text-blue">0</p>
                        </div>
                        <div class="col-8 align-self-center">
                            <input type="range" class="form-control-range color-grisclaro text-left py-2 mt-1"
                                id="calidadsalud" name="calidadsalud" min="0" max="100" step="10">
                        </div>
                        <div class="col-2 align-self-center">
                            <p class="mb-0 contenido-popups text-blue">100</p>
                        </div>
                    </div>

                    @if ($errors->has('calidadsalud'))
                        <div class="alert alert-danger">
                            {{ $errors->first('calidadsalud') }}
                        </div>
                    @endif
                </div>
            </div>
            <!-- FIN Calidad salud -->
            <!-- comentario -->
            <div class="row mt-4">
                <div class="col-12">
                    <p class="mb-0 titulos-encuesta text-blue"><b>¿Quieres comentar algo del programa de entrenamiento de
                            esta semana?</b></p>
                    <label for="comentario" class="d-none"></label>
                    <textarea class="form-control" id="comentario" name="comentario" rows="3" placeholder="Escribe aquí..."></textarea>
                </div>
            </div>
            <!-- FIN comentario-->
            <div class="row mt-4">
                <div class="col-12">
                    <input type="submit"
                        class="border-0 color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 text-white w-100"
                        value="Enviar">
                </div>
            </div>
    </div>
    </form>
    </div>
@stop
