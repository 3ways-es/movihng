@extends('layouts.app', ['home' => 1])
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/perfil.css') }}?x={{ rand(5, 15) }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/principal.css') }}?x={{ rand(5, 15) }}">
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/navbar.js') }}?x={{ rand(5, 15) }}"></script>
<script type="text/javascript" src="{{ asset('js/perfil.js') }}?x={{ rand(5, 15) }}"></script>
@endsection
@section('content')
<div class="container-fluid last-item">
   <div class="row pt-2">
         <div class="col-12 text-left">
            <a class="text-blue" href="{{ route('perfil') }}"><i class="fas fa-chevron-left"></i></a>
         </div>
   </div>
   <div class="row">
      <div class="col-12 my-4">
        <h2 class="text-blue">Calendario de citas</h2>
      </div>
   </div>
      @if (count($citas) > 0)
         @foreach ($citas as $indexmeses => $meses)
            <div class="color-blue newbuttons py-3 px-3 text-left mt-2 mb-3">
                  @php
                     switch ($indexmeses) {
                        case 'January':
                           $mesEs = 'Enero';
                           break;
                        case 'February':
                           $mesEs = 'Febrero';
                           break;
                        case 'March':
                           $mesEs = 'Marzo';
                           break;
                        case 'April':
                           $mesEs = 'Abril';
                           break;
                        case 'May':
                           $mesEs = 'Mayo';
                           break;
                        case 'June':
                           $mesEs = 'Junio';
                           break;
                        case 'July':
                           $mesEs = 'Julio';
                           break;
                        case 'August':
                           $mesEs = 'Agosto';
                           break;
                        case 'September':
                           $mesEs = 'Septiembre';
                           break;
                        case 'October':
                           $mesEs = 'Octubre';
                           break;
                        case 'November':
                           $mesEs = 'Noviembre';
                           break;
                        case 'December':
                           $mesEs = 'Diciembre';
                           break;
                     }
                  @endphp
                  <p class="mb-0 titulo-popups text-white"><b>{{ $mesEs }}</b></p>
            </div>
               <div class="table-responsive">
                  @php
                     $dias = array();
                     $colores = array();
                     $ids = array();
                     foreach($meses as $index => $value)
                     {
                        $dias[] = 'month_'.date('m', strtotime($value->fecha_cita)).'_day_'.date('d', strtotime($value->fecha_cita));
                        $colores[] = $value->color;
                        $ids[] = $value->id;
                     }
                  @endphp

                  {!! generar_calendario(date('m', strtotime($value->fecha_cita)), date('Y', strtotime($value->fecha_cita)), $dias, $colores, $ids); !!}
               </div>
         @endforeach
      @else
         <div class="row">
            <div class="col-12">
               <h2 class="text-blue">No tiene citas</h2>
            </div>
         </div>
      @endif
</div>
@stop

@section('modals')
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'popupcita', 
   'classTitulo' => 'js-titulo-popup',
   'footer' => 1,
   'contenido' => '<div class="js-contenido-popup"></div>'
   ]
)
@endsection

<?php
function generar_calendario($month,$year,$citas, $colores, $ids){

    $calendar = '<table class="table">';
 
      $headings = array('Lun','Mar','Mié','Jue','Vie','Sáb','Dom');
     
    $calendar.= '<tr><td scope="col">'.implode('</td><td class="calendar-day-head text-blue font-weight-bold">',$headings).'</td></tr>';
 
    $running_day = date('w',mktime(0,0,0,$month,1,$year));
    $running_day = ($running_day > 0) ? $running_day-1 : $running_day;
    $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
    $days_in_this_week = 1;
    $day_counter = 0;
    $dates_array = array();
 
    $calendar.= '<tr class="calendar-row">';
 
    for($x = 0; $x < $running_day; $x++):
        $calendar.= '<td class="calendar-day-np"> </td>';
        $days_in_this_week++;
    endfor;
 
    for($list_day = 1; $list_day <= $days_in_month; $list_day++):
        $calendar.= '<td class="text-center text-blue">';
         
        $class="day-number ";
        if($running_day == 5 || $running_day == 6 ){
            $class.=" text-grey ";
        }
         
         if($list_day < 10)
         {
         $key_month_day = "month_{$month}_day_0{$list_day}";
         }
         else
         {
            $key_month_day = "month_{$month}_day_{$list_day}";
         }
         $idcodifiado = '';
        if($citas != null && is_array($citas)){
            $month_key = array_search($key_month_day, $citas);
            if(is_numeric($month_key)){
                $class.="cursor-pointer dia-cita text-white color-".$colores[$month_key]."";
                $idcodifiado = base64_encode($ids[$month_key]);
            }
        }
         if($idcodifiado != '')
         {
            $calendar.= "<div class='{$class} js-cita' data-info='{$idcodifiado}' data-toggle='modal' data-target='#popupcita'>".$list_day."</div>";
         }
         else{
            $calendar.= "<div class='{$class}'>".$list_day."</div>";
         }
         
             
        $calendar.= '</td>';
        if($running_day == 6):
            $calendar.= '</tr>';
            if(($day_counter+1) != $days_in_month):
                $calendar.= '<tr class="calendar-row">';
            endif;
            $running_day = -1;
            $days_in_this_week = 0;
        endif;
        $days_in_this_week++; $running_day++; $day_counter++;
    endfor;
 
    if($days_in_this_week < 8):
        for($x = 1; $x <= (8 - $days_in_this_week); $x++):
            $calendar.= '<td class="calendar-day-np"> </td>';
        endfor;
    endif;
 
    $calendar.= '</tr>';
 
    $calendar.= '</table>';
     
    return $calendar;
}
 
?>
