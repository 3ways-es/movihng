@extends('layouts.app')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/principal.css') }}?x={{ rand(5, 15) }}">
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/principal.js') }}?x={{ rand(5, 15) }}"></script>
@endsection
@section('content')
   <div class="container-fluid mt-4">
      <div class="row">
            <div class="col-12 col-md-5 align-self-center">
               <img src="{{ asset('images/entrenamiento/programa'.$programa.'/principal.png') }}" align="middle" class="img-fluid">
            </div>
            <div class="col-12 col-md-7">
               <p class="text-blue titulos-popups mb-1">Programa {{ $programa }}</p>
               <div class="{{ $programa == 2 ? 'color-green' : 'color-yellow' }} py-4 px-3 newbuttons botones-principal mb-2 popups" data-toggle="modal" data-target="#pm{{ $programa }}">
                  <div class="container-fluid">
                     <div class="row">
                        <div class="col-2 pr-0">
                           @if ($programa == 2)
                              <img src="{{ asset('images/icons/icon-programas-2.svg') }}" align="middle" class="w-100 estandar-iconos">
                           @else
                              <img src="{{ asset('images/icons/icon-programas-1.svg') }}" align="middle" class="w-100 estandar-iconos">
                           @endif
                        </div>
                        <div class="col-10 align-self-center">
                           <p class="mb-0 align-self-center text-blue contenido-popups "><b>Planificación mensual</b></p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="{{ $programa == 3 ? 'color-green' : 'color-coral' }} py-4 px-3 newbuttons botones-principal mb-2 popups" data-toggle="modal" data-target="#ce{{ $programa }}">
                  <div class="container-fluid">
                     <div class="row">
                        <div class="col-2 pr-0">
                           @if ($programa == 3)
                              <img src="{{ asset('images/icons/icon-programas-4.svg') }}" align="middle" class="w-100 estandar-iconos">
                           @else
                              <img src="{{ asset('images/icons/icon-programas-3.svg') }}" align="middle" class="w-100 estandar-iconos">
                           @endif
                        </div>
                        <div class="col-10 align-self-center">
                           <p class="mb-0 text-blue align-self-center  contenido-popups "><b>Circuitos de ejercicios</b></p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="color-blue py-4 px-3 newbuttons botones-principal mb-3 popups" data-toggle="modal" data-target="#ap{{ $programa }}">
                  <div class="container-fluid">
                     <div class="row">
                        <div class="col-2 pr-0">
                           <img src="{{ asset('images/icons/icon-programas-5.svg') }}" align="middle" class="w-100 estandar-iconos ">
                        </div>
                        <div class="col-10 align-self-center">
                           <p class="mb-0 text-white align-self-center text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
      </div>
   </div>
@stop

@section('modals')
@if ($programa == 1)
<!-- Circuitos de ejercicios -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce1',
   'titulo' => 'Programa 1',
   'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
               <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa1/ejercicio1.png').'" align="middle" class="w-100 mb-3">
                        <button class="color-green newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button" data-toggle="collapse" data-target="#circuito11" aria-expanded="false" aria-controls="circuito11">Ejercicios Circuito 1</button>
                        <div class="collapse multi-collapse" id="circuito11">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce11"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce12"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce13"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce14"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce15"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce16"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce17"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa1/ejercicio2.png').'" align="middle" class="w-100 mb-3">
                        <button class="color-green newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button" data-toggle="collapse" data-target="#circuito12" aria-expanded="false" aria-controls="circuito12">Ejercicios Circuito 2</button>
                        <div class="collapse multi-collapse" id="circuito12">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce21"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce22"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce23"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce24"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce25"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce26"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce27"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa1/ejercicio3.png').'" align="middle" class="w-100 mb-3">
                        <button class="color-green newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button" data-toggle="collapse" data-target="#circuito13" aria-expanded="false" aria-controls="circuito13">Ejercicios Circuito 3</button>
                        <div class="collapse multi-collapse" id="circuito13">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce31"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce32"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce33"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce34"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce35"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce36"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce37"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                  </div>
               </div>
              '
   ]
)
<!-- Circuito 1 -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce11', 
   'titulo' => 'Sentadillas',
   'contenido' => '
   <div class="embed-responsive embed-responsive-1by1">
   <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026029" frameborder="0" scrolling="no" allowfullscreen></iframe>
   </div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce12', 
   'titulo' => 'Equilibrio',
   'contenido' => '
    <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026032" frameborder="0" scrolling="no" allowfullscreen></iframe>
    </div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce13', 
   'titulo' => 'Pecho',
   'contenido' => '
    <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026060" frameborder="0" scrolling="no" allowfullscreen></iframe>
    </div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce14', 
   'titulo' => 'Tronco inferior',
   'contenido' => '
    <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026096" frameborder="0" scrolling="no" allowfullscreen></iframe>
    </div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce15', 
   'titulo' => 'Glúteo',
   'contenido' => '
      <div class="embed-responsive embed-responsive-1by1">
      <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026115" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce16', 
   'titulo' => 'Abdomen',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026150" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce17', 
   'titulo' => 'Espalda',
   'footer' => 1,
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649026180" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
<!-- FIN Circuito 1 -->

<!-- Circuito 2 -->

@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce21', 
   'titulo' => 'Sentadillas',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032659" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce22', 
   'titulo' => 'Equilibrio',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032701" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce23', 
   'titulo' => 'Pecho',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032723" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce24', 
   'titulo' => 'Tronco inferior',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032744" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce25', 
   'titulo' => 'Glúteo',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032766" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce26', 
   'titulo' => 'Abdomen',
   'contenido' => '<div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032792" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce27', 
   'titulo' => 'Espalda',
   'contenido' => '<div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649032822" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
<!-- FIN Circuito 2 -->

<!-- Circuito 3 -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce31', 
   'titulo' => 'Sentadillas',
   'contenido' => '<div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046523" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce32', 
   'titulo' => 'Equilibrio',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046570" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce33', 
   'titulo' => 'Pecho',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046624" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce34', 
   'titulo' => 'Tronco inferior',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046667" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce35', 
   'titulo' => 'Glúteo',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item"src="https://player.vimeo.com/video/649046700" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce36', 
   'titulo' => 'Abdomen',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046783" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce37', 
   'titulo' => 'Espalda',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649046785" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
<!-- FIN Circuito 3 -->
<!-- FIN Circuitos de ejercicios -->

<!-- Ejercicios aerobicos -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'ap1', 
   'titulo' => 'Programa 1',
   'customDialog' => 'especialdesk', 
   'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
              <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
                <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
              </div>
              <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar: <b>10.000 pasos</b></p>
              <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
              <div class="container-fluid">
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales <b>al menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b></p>
                  </div>
                </div>
              </div>'
   ]
)
<!-- FIN Ejercicios aerobicos -->

<!-- Planificación mensual --> 
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'pm1', 
   'titulo' => 'Programa 1',
   'customDialog' => 'especialdesk', 
   'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
              <div class="container-fluid">
                <div class="row">
                  <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="1" data-mes="1">
                    <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-1-1"><b>Mes 1</b></p>
                  </div>
                  <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="1" data-mes="2">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-1-2"><b>Mes 2</b></p>
                  </div>
                  <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="1" data-mes="3">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-1-3"><b>Mes 3</b></p>
                  </div>
                </div>
              </div>
              <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
                <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
                <p class="mb-0 contenido-popups text-white">Mar</p>
                <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
                <p class="mb-0 contenido-popups text-white">Jue</p>
                <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
                <p class="mb-0 contenido-popups text-white">Sáb</p>
                <p class="mb-0 contenido-popups text-white">Dom</p>
              </div>
              <div class="js-mes-1-1">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                     </div>
                     <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                     </div>
                     <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                            <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                            <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                            <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                        </div>
                     </div>
                  </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-4">
                       <div class="color-yellow d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-green d-block px-2 py-1 ">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                  </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-4">
                       <div class="color-coral d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                  </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-4">
                       <div class="color-green d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                  </div>
                </div>
                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
                <div class="container-fluid mb-4">
                  <div class="row">
                    <div class="col-5 explicacion py-2">
                      <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                      <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                      <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                      <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                    </div>
                    <div class="col-7">
                      <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe realizar en este día.</b></p>
                      <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que se realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los segundos sobre cada pie.</b></p>
                      <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso entre cada ejercicio.</b></p>
                      <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que debemos realizar el circuito de ejercicios completo.</b></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="js-mes-1-2" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-4">
                       <div class="color-green d-block px-2 py-1">
                      <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                      <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                    </div>
                     </div>
                     <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                      <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                      <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                    </div>
                     </div>
                     <div class="col-4">
                         <div class="color-coral d-block px-2 py-1">
                      <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                      <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                    </div>
                  </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-4">
                       <div class="color-yellow d-block px-2 py-1">
                      <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                      <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                    </div>
                     </div>
                     <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                      <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                      <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                    </div>
                     </div>
                     <div class="col-4">
                         <div class="color-green d-block px-2 py-1">
                      <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                      <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">60s descanso</p>
                      <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                    </div>
                  </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-4">
                       <div class="color-coral d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-green d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                  </div>
                </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-4">
                       <div class="color-green d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-yellow d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                     <div class="col-4">
                        <div class="color-coral d-block px-2 py-1">
                         <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                         <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                         <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                       </div>
                     </div>
                  </div>
                </div>
                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
                  </div>
                </div>
              </div>
              <div class="js-mes-1-3" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-yellow d-block px-2 py-1 ">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                      <div class="row p-0">
                          <div class="col-4">
                  <div class="color-yellow d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                          <div class="col-4">
                  <div class="color-coral d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                          <div class="col-4">
                  <div class="color-green d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                      </div>
                  </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                      <div class="row p-0">
                          <div class="col-4">
                  <div class="color-coral d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                          <div class="col-4">
                  <div class="color-green d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                          <div class="col-4">
                  <div class="color-yellow d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                      </div>
                  </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">

               <div class="color-green d-block px-2 py-1 w-100">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-yellow d-block px-2 py-1 w-100">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-coral d-block px-2 py-1 w-100">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>

                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
              </div>'
   ]
)
<!-- FIN Planificación mensual --> 

@elseif($programa == 2)
<!-- Circuitos de ejercicios -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce2', 
   'titulo' => 'Programa 2',
   'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
   <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa2/ejercicio1.png').'" align="middle" class="w-100 mb-3">
                        <button class="color-yellow newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button" data-toggle="collapse" data-target="#circuito21" aria-expanded="false" aria-controls="circuito21">Ejercicios Circuito 1</button>
                        <div class="collapse multi-collapse" id="circuito21">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce11"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce12"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce13"><b>Hombro</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce14"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce15"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce16"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce17"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa2/ejercicio2.png').'" align="middle" class="w-100 mb-3">
                        <button class="color-yellow newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button" data-toggle="collapse" data-target="#circuito22" aria-expanded="false" aria-controls="circuito22">Ejercicios Circuito 2</button>
                        <div class="collapse multi-collapse" id="circuito22">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce21"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce22"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce23"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce24"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce25"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce26"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce27"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa2/ejercicio3.png').'" align="middle" class="w-100 mb-3">
                        <button class="color-yellow newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button" data-toggle="collapse" data-target="#circuito23" aria-expanded="false" aria-controls="circuito23">Ejercicios Circuito 3</button>
                        <div class="collapse multi-collapse" id="circuito23">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce31"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce32"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce33"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce34"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce35"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce36"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce37"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                  </div>
               </div>
              '
   ]
)
<!-- Circuito 1 -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce11', 
   'titulo' => 'Sentadillas',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085824" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce12', 
   'titulo' => 'Equilibrio',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085928" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce13', 
   'titulo' => 'Hombro',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085918" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce14', 
   'titulo' => 'Tronco inferior',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649085968" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce15', 
   'titulo' => 'Glúteo',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649086003" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce16', 
   'titulo' => 'Abdomen',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649086116" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce17', 
   'titulo' => 'Espalda',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649086089" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
<!-- FIN Circuito 1 -->

<!-- Circuito 2 -->

@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce21', 
   'titulo' => 'Sentadillas',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649094982" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce22', 
   'titulo' => 'Equilibrio',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095072" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce23', 
   'titulo' => 'Pecho',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095073" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce24', 
   'titulo' => 'Tronco inferior',
   'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095117" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce25', 
   'titulo' => 'Glúteo',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095158" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce26', 
   'titulo' => 'Abdomen',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095217" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'   
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce27', 
   'titulo' => 'Espalda',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649095251" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'   
    ]
)
<!-- FIN Circuito 2 -->

<!-- Circuito 3 -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce31', 
   'titulo' => 'Sentadillas',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100788" frameborder="0" scrolling="no" allowfullscreen></iframe></div>' 
       ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce32', 
   'titulo' => 'Equilibrio',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100860" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'    
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce33', 
   'titulo' => 'Pecho',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100875" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce34', 
   'titulo' => 'Tronco inferior',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100940" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce35', 
   'titulo' => 'Glúteo',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649100972" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce36', 
   'titulo' => 'Abdomen',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649101006" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce37', 
   'titulo' => 'Espalda',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649101058" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'   ]
)
<!-- FIN Circuito 3 -->
<!-- FIN Circuitos de ejercicios -->

<!-- Ejercicios aerobicos -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'ap2', 
   'titulo' => 'Programa 2',
   'customDialog' => 'especialdesk', 
   'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
              <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
                <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
              </div>
              <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000 pasos</b></p>
              <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
              <div class="container-fluid">
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales <b>al menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b></p>
                  </div>
                </div>
              </div>'
   ]
)
<!-- FIN Ejercicios aerobicos -->

<!-- Planificación mensual --> 
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'pm2', 
   'titulo' => 'Programa 2',
   'customDialog' => 'especialdesk', 
   'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
              <div class="container-fluid">
                <div class="row">
                  <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="2" data-mes="1">
                    <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-2-1"><b>Mes 1</b></p>
                  </div>
                  <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="2" data-mes="2">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-2-2"><b>Mes 2</b></p>
                  </div>
                  <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="2" data-mes="3">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-2-3"><b>Mes 3</b></p>
                  </div>
                </div>
              </div>
              <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
                <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
                <p class="mb-0 contenido-popups text-white">Mar</p>
                <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
                <p class="mb-0 contenido-popups text-white">Jue</p>
                <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
                <p class="mb-0 contenido-popups text-white">Sáb</p>
                <p class="mb-0 contenido-popups text-white">Dom</p>
              </div>
              <div class="js-mes-2-1">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">

               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">

               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-green d-block px-2 py-1 ">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">

               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>

                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">

               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
                <div class="container-fluid mb-4">
                  <div class="row">
                    <div class="col-5 explicacion py-2">
                      <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                      <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                      <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                      <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                    </div>
                    <div class="col-7">
                      <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe realizar en este día.</b></p>
                      <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que se realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los segundos sobre cada pie.</b></p>
                      <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso entre cada ejercicio.</b></p>
                      <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que debemos realizar el circuito de ejercicios completo.</b></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="js-mes-2-2" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                                   </div>
                   </div>
               </div>

                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                                   </div>
                   </div>
               </div>

                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                               </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                               </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                               </div>
                       </div>
                   </div>
               </div>

                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                               </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
              </div>
              <div class="js-mes-2-3" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1 ">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x2 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
              </div>'
   ]
)
<!-- FIN Planificación mensual --> 

@elseif($programa == 3)
<!-- Circuitos de ejercicios -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce3', 
   'titulo' => 'Programa 3',
   'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
   <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa3/ejercicio1.png').'" align="middle" class="w-100 mb-2">
                        <button class="color-coral newbuttons w-100 mb-3 py-3 border-0 text-blue cursor-pointer" type="button" data-toggle="collapse" data-target="#circuito31" aria-expanded="false" aria-controls="circuito31">Ejercicios Circuito 1</button>
                        <div class="collapse multi-collapse" id="circuito31">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce11"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce12"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce13"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce14"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce15"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce16"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce17"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa3/ejercicio2.png').'" align="middle" class="w-100 mb-2">
                        <button class="color-coral newbuttons w-100 mb-3 py-3 border-0 text-blue" type="button" data-toggle="collapse" data-target="#circuito32" aria-expanded="false" aria-controls="circuito32">Ejercicios Circuito 2</button>
                        <div class="collapse multi-collapse" id="circuito32">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce21"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce22"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce23"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce24"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce25"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce26"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce27"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa3/ejercicio3.png').'" align="middle" class="w-100  mb-2">
                        <button class="color-coral newbuttons w-100 mb-3 py-3 border-0 text-blue" type="button" data-toggle="collapse" data-target="#circuito33" aria-expanded="false" aria-controls="circuito33">Ejercicios Circuito 3</button>
                        <div class="collapse multi-collapse" id="circuito33">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce31"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce32"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce33"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce34"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce35"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce36"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce37"><b>Hombro</b></li>
                            </ol>
                        </div>
                     </div>
                  </div>
               </div>
              '
   ]
)
<!-- Circuito 1 -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce11', 
   'titulo' => 'Sentadillas',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104751" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce12', 
   'titulo' => 'Equilibrio',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104547" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce13', 
   'titulo' => 'Pecho',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104682" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce14', 
   'titulo' => 'Tronco inferior',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104793" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce15', 
   'titulo' => 'Glúteo',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104655" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce16', 
   'titulo' => 'Abdomen',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104511" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce17', 
   'titulo' => 'Espalda',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649104611" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
<!-- FIN Circuito 1 -->

<!-- Circuito 2 -->

@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce21', 
   'titulo' => 'Sentadillas',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111255" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce22', 
   'titulo' => 'Equilibrio',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111065" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce23', 
   'titulo' => 'Pecho',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111208" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce24', 
   'titulo' => 'Tronco inferior',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111316" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce25', 
   'titulo' => 'Glúteo',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111161" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce26', 
   'titulo' => 'Abdomen',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111017" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce27', 
   'titulo' => 'Espalda',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649111108" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
<!-- FIN Circuito 2 -->

<!-- Circuito 3 -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce31', 
   'titulo' => 'Sentadillas',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115863" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce32', 
   'titulo' => 'Equilibrio',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115654" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce33', 
   'titulo' => 'Hombro',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115924" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce34', 
   'titulo' => 'Tronco inferior',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115913" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce35', 
   'titulo' => 'Glúteo',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115755" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce36', 
   'titulo' => 'Abdomen',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115618" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce37', 
   'titulo' => 'Espalda',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/649115701" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
<!-- FIN Circuito 3 -->
<!-- FIN Circuitos de ejercicios -->

<!-- Ejercicios aerobicos -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'ap3', 
   'titulo' => 'Programa 3',
   'customDialog' => 'especialdesk', 
   'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
              <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
                <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
              </div>
              <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000 pasos</b></p>
              <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
              <div class="container-fluid">
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales <b>al menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b></p>
                  </div>
                </div>
              </div>'
   ]
)
<!-- FIN Ejercicios aerobicos -->
<!-- Planificación mensual --> 
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'pm3', 
   'titulo' => 'Programa 3',
   'customDialog' => 'especialdesk', 
   'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
              <div class="container-fluid">
                <div class="row">
                  <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="3" data-mes="1">
                    <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-3-1"><b>Mes 1</b></p>
                  </div>
                  <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="3" data-mes="2">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-3-2"><b>Mes 2</b></p>
                  </div>
                  <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="3" data-mes="3">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-3-3"><b>Mes 3</b></p>
                  </div>
                </div>
              </div>
              <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
                <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
                <p class="mb-0 contenido-popups text-white">Mar</p>
                <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
                <p class="mb-0 contenido-popups text-white">Jue</p>
                <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
                <p class="mb-0 contenido-popups text-white">Sáb</p>
                <p class="mb-0 contenido-popups text-white">Dom</p>
              </div>
              <div class="js-mes-3-1">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1 ">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
                <div class="container-fluid mb-4">
                  <div class="row">
                    <div class="col-5 explicacion py-2">
                      <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                      <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                      <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                      <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                    </div>
                    <div class="col-7">
                      <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe realizar en este día.</b></p>
                      <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que se realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los segundos sobre cada pie.</b></p>
                      <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso entre cada ejercicio.</b></p>
                      <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que debemos realizar el circuito de ejercicios completo.</b></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="js-mes-3-2" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
               <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
              </div>
              <div class="js-mes-3-3" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1 ">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                  <div class="container-fluid p-0">
                      <div class="row p-0">
                          <div class="col-4">
                                  <div class="color-green d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                          <div class="col-4">
                                  <div class="color-yellow d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                          <div class="col-4">
                                  <div class="color-coral d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                      </div>
                  </div>
               <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
              </div>'
   ]
)
<!-- FIN Planificación mensual --> 

@else
<!-- Circuitos de ejercicios -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce4', 
   'titulo' => 'Programa 4',
   'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Circuito de ejercicios</b></p>
   <div class="container-fluid p-0">
                  <div class="row p-0">
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa4/ejercicio1.png').'" align="middle" class="w-100 mb-2">
                        <button class="color-blue newbuttons w-100 mb-3 py-3 border-0 text-white cursor-pointer" type="button" data-toggle="collapse" data-target="#circuito41" aria-expanded="false" aria-controls="circuito41">Ejercicios Circuito 1</button>
                        <div class="collapse multi-collapse" id="circuito41">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce11"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce12"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce13"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce14"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce15"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce16"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce17"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa4/ejercicio2.png').'" align="middle" class="w-100 mb-2">
                        <button class="color-blue newbuttons w-100 mb-3 py-3 border-0 text-white" type="button" data-toggle="collapse" data-target="#circuito42" aria-expanded="false" aria-controls="circuito42">Ejercicios Circuito 2</button>
                        <div class="collapse multi-collapse" id="circuito42">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce21"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce22"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce23"><b>Hombros</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce24"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce25"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce26"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce27"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                     <div class="col-12 col-md-4 mb-3">
                        <img src="'.asset('images/entrenamiento/programa4/ejercicio3.png').'" align="middle" class="w-100  mb-2">
                        <button class="color-blue newbuttons w-100 mb-3 py-3 border-0 text-white" type="button" data-toggle="collapse" data-target="#circuito43" aria-expanded="false" aria-controls="circuito43">Ejercicios Circuito 3</button>
                        <div class="collapse multi-collapse" id="circuito43">
                             <ol class="pl-4">
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce31"><b>Sentadillas</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce32"><b>Equilibrio</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce33"><b>Pecho</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce34"><b>Tronco inferior</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce35"><b>Glúteo</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce36"><b>Abdomen</b></li>
                                <li class="text-blue contenido-popups mb-1 cursor-pointer" data-toggle="modal" data-target="#ce37"><b>Espalda</b></li>
                            </ol>
                        </div>
                     </div>
                  </div>
               </div>
              '
   ]
)
<!-- Circuito 1 -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce11', 
   'titulo' => 'Sentadillas',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705206" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce12', 
   'titulo' => 'Equilibrio',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705224" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce13', 
   'titulo' => 'Pecho',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705243" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce14', 
   'titulo' => 'Tronco inferior',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705265" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce15', 
   'titulo' => 'Glúteo',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705285" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce16', 
   'titulo' => 'Abdomen',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705307" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce17', 
   'titulo' => 'Espalda',
    'contenido' => '   <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705328" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
    ]
)
<!-- FIN Circuito 1 -->

<!-- Circuito 2 -->

@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce21', 
   'titulo' => 'Sentadillas',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705344" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce22', 
   'titulo' => 'Equilibrio',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705360" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce23', 
   'titulo' => 'Hombros',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705380" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce24', 
   'titulo' => 'Tronco inferior',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705393" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce25', 
   'titulo' => 'Glúteo',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705409" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce26', 
   'titulo' => 'Abdomen',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705418" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce27', 
   'titulo' => 'Espalda',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705430" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
<!-- FIN Circuito 2 -->

<!-- Circuito 3 -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce31', 
   'titulo' => 'Sentadillas',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705455" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce32', 
   'titulo' => 'Equilibrio',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705474" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce33', 
   'titulo' => 'Pecho',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705489" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce34', 
   'titulo' => 'Tronco inferior',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705521" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce35', 
   'titulo' => 'Glúteo',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705535" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce36', 
   'titulo' => 'Abdomen',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705549" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white',
   'customDialog' => 'especialdesk', 
   'modalID' => 'ce37', 
   'titulo' => 'Espalda',
   'contenido' => '  <div class="embed-responsive embed-responsive-1by1">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/716705571" frameborder="0" scrolling="no" allowfullscreen></iframe></div>'
   ]
)
<!-- FIN Circuito 3 -->
<!-- FIN Circuitos de ejercicios -->

<!-- Ejercicios aerobicos -->
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'ap4', 
   'titulo' => 'Programa 4',
   'customDialog' => 'especialdesk', 
   'contenido' => '<p class="mb-0 text-blue contenido-popups "><b>Ejercicio aeróbico</b></p>
              <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3">
                <p class="mb-0 contenido-popups text-white"><b>Objetivo alcanzar:</b> 7.500 pasos/diarios</p>
              </div>
              <p class="mb-0 text-blue contenido-popups ">Al menos <b>2 días a la semana</b> alcanzar:<br><b>10.000 pasos</b></p>
              <p class="mb-0 text-blue contenido-popups mt-3 mb-2"><b>Progresión</b></p>
              <div class="container-fluid">
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-yellow newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 1</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue">10.000 pasos <b>2 días a la semana.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-green newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 2</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>11.000 pasos 2 días a la semana</b> de los cuales <b>al menos 2.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-coral newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 3</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al menos 3.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-blue newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 4</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>12.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-cyan newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 5</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 100 - 110 lpm.</b></p>
                  </div>
                </div>
                <div class="row mb-3">
                  <div class="col-3 p-0">
                    <div class="color-grey newbuttons mesesaerobico py-2 px-2 text-center mt-2 mb-3">
                      <p class="mb-0 contenido-popups text-white"><b>Mes 6</b></p>
                    </div>
                  </div>
                  <div class="col-9">
                    <p class="mb-0 contenido-popups text-blue"><b>13.000 pasos 2 días a la semana</b> de los cuales <b>al menos 4.000</b> tienen que ser con una <b>FC > 110 lpm.</b></p>
                  </div>
                </div>
              </div>'
   ]
)
<!-- FIN Ejercicios aerobicos -->
<!-- Planificación mensual --> 
@include('modals.simple-modal-principal', 
   [
   'customClass' => 'bg-white', 
   'modalID' => 'pm4', 
   'titulo' => 'Programa 4',
   'customDialog' => 'especialdesk', 
   'contenido' => '<p class="mb-3 text-blue contenido-popups "><b>Planificación mensual</b></p>
              <div class="container-fluid">
                <div class="row">
                  <div class="col-4 p-0 text-left botones-principal js-mes" data-programa="4" data-mes="1">
                    <p class="mb-3 text-blue contenido-popups meses-activo js-mesactivo-4-1"><b>Mes 1</b></p>
                  </div>
                  <div class="col-4 p-0 text-center botones-principal js-mes" data-programa="4" data-mes="2">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-4-2"><b>Mes 2</b></p>
                  </div>
                  <div class="col-4 p-0 text-right botones-principal js-mes" data-programa="4" data-mes="3">
                    <p class="mb-3 text-blue contenido-popups js-mesactivo-4-3"><b>Mes 3</b></p>
                  </div>
                </div>
              </div>
              <div class="color-blue newbuttons py-3 px-3 text-center mt-2 mb-3 w-100 d-flex dias-semana">
                <p class="mb-0 contenido-popups text-white"><b>Lun</b></p>
                <p class="mb-0 contenido-popups text-white">Mar</p>
                <p class="mb-0 contenido-popups text-white"><b>Mié</b></p>
                <p class="mb-0 contenido-popups text-white">Jue</p>
                <p class="mb-0 contenido-popups text-white"><b>Vie</b></p>
                <p class="mb-0 contenido-popups text-white">Sáb</p>
                <p class="mb-0 contenido-popups text-white">Dom</p>
              </div>
              <div class="js-mes-4-1">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups  titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1 ">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">9 - 11 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
                <div class="container-fluid mb-4">
                  <div class="row">
                    <div class="col-5 explicacion py-2">
                      <p class="text-blue contenido-popups mb-3"><b>Circuito 1</b></p>
                      <p class="text-green contenido-popups mb-3"><b>11-12 repeticiones</b></p>
                      <p class="text-coral contenido-popups mb-3"><b>45s descanso</b></p>
                      <p class="text-cyan contenido-popups mb-3"><b>x2 vueltas</b></p>
                    </div>
                    <div class="col-7">
                      <p class="text-blue contenido-popups mb-3 contenido-bloques-semana"><b>Circuito que se debe realizar en este día.</b></p>
                      <p class="text-green contenido-popups mb-3 contenido-bloques-semana"><b>Nºde repeticiones que se realiza en cada ejercicio. En el caso de los ejercicios de equilibrio son los segundos sobre cada pie.</b></p>
                      <p class="text-coral contenido-popups mb-3 contenido-bloques-semana"><b>Tiempo de descanso entre cada ejercicio.</b></p>
                      <p class="text-cyan contenido-popups mb-3 contenido-bloques-semana"><b>Número de veces que debemos realizar el circuito de ejercicios completo.</b></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="js-mes-4-2" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">12 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
               <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">11 - 12 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
              </div>
              <div class="js-mes-4-3" style="display: none;">
                <p class="text-blue contenido-popups mb-1 titulos-bloques-semana"><b>Semana 1</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1 ">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 2</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">18 - 20 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 3</b></p>
                <div class="container-fluid p-0">
                   <div class="row p-0">
                       <div class="col-4">
                               <div class="color-coral d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-green d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                       <div class="col-4">
                               <div class="color-yellow d-block px-2 py-1">
                                     <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                     <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">13 - 15 repeticiones</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                     <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                   </div>
                       </div>
                   </div>
               </div>
                <p class="text-blue contenido-popups mb-1 mt-2 titulos-bloques-semana"><b>Semana 4</b></p>
                  <div class="container-fluid p-0">
                      <div class="row p-0">
                          <div class="col-4">
                                  <div class="color-green d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 1</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">45s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                          <div class="col-4">
                                  <div class="color-yellow d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 2</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                          <div class="col-4">
                                  <div class="color-coral d-block px-2 py-1">
                                        <p class="mb-1 text-blue contenido-popups titulos-bloques-semana"><b>Circuito 3</b></p>
                                        <p class="mb-1 text-blue contenido-popups contenido-bloques-semana ">15 repeticiones</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">30s descanso</p>
                                        <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">x3 vueltas</p>
                                      </div>
                          </div>
                      </div>
                  </div>
               <div class="w-100 text-right mt-2">
                  <p class="mb-0 text-blue contenido-popups contenido-bloques-semana ">Viernes opcional</p>
                </div>
              </div>'
   ]
)
<!-- FIN Planificación mensual --> 
@endif

@endsection
