
@extends('layouts.app', ['home' => 1])

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/principal.css') }}?x={{ rand(5, 15) }}">
@endsection

@section('content')
<div id="app">
  <div class="container last-item py-5">
      <div class="row">
        <div class="col-12">
          <groups :initial-groups="{{ $groups }}" :user="{{ $user }}" :initial-notifications="{{ $notifications }}"></groups>
        </div>
        @if (\Auth::user()->role_id != 2)
          <div class="col-12">
            <create-group :initial-users="{{ $users }}" :user="{{ $user }}"></create-group>
          </div>
          <div class="col-12">
            <broadcast-message :user="{{ $user }}"></broadcast-message>
          </div>
        @endif
      </div>
  </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/navbar.js') }}"></script>
@endsection