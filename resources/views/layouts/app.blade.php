<!doctype html>
<html lang="es">
<head>
   @include('includes.head')
</head>
<body>
    @if(@$home)
   @include('includes.navbar')
    @endif
    {{--  
   <header class="row">
       @include('includes.header')
   </header>
   --}}
    <main role="main">
        @yield('content')
    </main>
   {{--  
   <footer class="row">
       @include('includes.footer')
   </footer>
   --}}
   @yield('modals')
</body>
</html>