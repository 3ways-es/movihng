<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//ES" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="initial-scale=1.0" />
</head>

<body style="margin: 0; padding: 0;">
    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0" style="width: 100%; margin: 0; padding: 0;">
        <tbody>
            <tr>
                <td align="center">
                    <table width="600" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; text-align: left; margin: 0 auto;">
                        <tbody>
                            <tr>
                                <td align="center">
                                    <a href="{{ env('APP_URL') }}"><img src="{{ env('APP_URL') }}/images/mail/mail.png" alt="Movhing" style="width: 600px"></a>
                                </td>
                            </tr>
                            <tr style="height: 15px;">
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <p style="max-width: 550px; margin-bottom: 0; text-align: left; font-size: 16px; font-weight: bold; color: #0a0143;
                                    font-family: 'Roboto Condensed', sans-serif"><b>Hola {{ $data->codigo }},</b></p>
                                </td>
                            </tr>
                            <tr style="height: 15px;">
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <p style="max-width: 550px; margin-bottom: 0; text-align: left; font-size: 16px; font-weight: normal; color: #0a0143;
                                    font-family: 'Roboto Condensed', sans-serif">Para poder <b>obtener una contraseña nueva</b>, por favor, pulse en el siguiente enlace:
                                    </p>
                                </td>
                            </tr>
                            <tr style="height: 15px;">
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="{{ route('reset-password', ['key' => $data->key, 'codigo' => $data->codigo]) }}" style="color: lightskyblue;">Recuperar Contraseña</a>
                                </td>
                            </tr>
                            <tr style="height: 15px;">
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <p style="max-width: 550px; margin-bottom: 0; text-align: left; font-size: 16px; font-weight: normal; color: #0a0143;
                                    font-family: 'Roboto Condensed', sans-serif">Si no ha solicitado la recuperación de contraseña, no haga caso a este email.
                                    </p>
                                </td>
                            </tr>
                            <tr style="height: 15px;">
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <p style="max-width: 550px; margin-bottom: 0; text-align: left; font-size: 16px; font-weight: normal; color: #0a0143;
                                    font-family: 'Roboto Condensed', sans-serif">Muchas gracias,
                                    </p>
                                </td>
                            </tr>
                            <tr style="height: 5px;">
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <p style="max-width: 550px; margin-bottom: 0; text-align: left; font-size: 16px; font-weight: normal; color: #0a0143;
                                    font-family: 'Roboto Condensed', sans-serif; margin-top: 0;">Un cordial saludo
                                    </p>
                                </td>
                            </tr>
                            <tr style="height: 15px;">
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>