Hola {{ $data->codigo }},
Para poder <b>obtener una contraseña nueva</b>, por favor, pulse en el siguiente enlace:
{{ route('reset-password', ['key' => $data->key, 'codigo' => $data->codigo]) }}
Si no ha solicitado la recuperación de contraseña, no haga caso a este email.
Muchas gracias,
Un cordial saludo