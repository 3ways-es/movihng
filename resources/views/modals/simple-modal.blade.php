<div class="modal fade" id="{{ $modalID }}" tabindex="-1" role="dialog" aria-labelledby="{{ $modalID }}" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content {{ @$customClass }}">
      <div class="modal-header border-0">
        {!! @$titulo !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! @$contenido !!}
      </div>
    </div>
  </div>
</div>