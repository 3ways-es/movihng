<div class="modal fade" id="{{ $modalID }}" tabindex="-1" role="dialog" aria-labelledby="{{ $modalID }}" aria-hidden="true">
  <div class="modal-dialog {{ @$customDialog }} modal-dialog-centered" role="document">
    <div class="modal-content {{ @$customClass }}">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pt-0">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-12">
               <h3 class="text-blue titulos-popups mb-0 {{ @$classTitulo }}">{!! @$titulo !!}</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              {!! @$contenido !!}
            </div>
          </div>
        </div>
      </div>
      @if (@$footer)
      <div class="modal-footer">
        <div class="container-fluid">
          <div class="navbar  navbar-dark bg-white">
            <a class="navbar-brand mr-0 {{ Route::current()->getName() == 'principal' || Route::current()->getName() == 'home' ? 'text-green' : 'text-grey' }}" href="{{ route('principal') }}"><i class="fas fa-home"></i></a>
            <div class="d-block">
                <a class="navbar-brand mr-0 {{ Route::current()->getName() == 'citas' ? 'text-green' : 'text-grey' }}" href="{{ route('citas') }}"><i class="fas fa-bell"></i>
                  <span class="js-notificaciones position-absolute"></span></a>
            </div>
            <div class="d-block">
                <a class="navbar-brand mr-0 {{ Route::current()->getName() == 'chat' ? 'text-green' : 'text-grey' }}" href="{{ route('chat') }}"><i class="fas fa-comment"></i>
                  <span class="js-notificaciones-2 position-absolute"></span>
                </a>
            </div>
            <a class="navbar-brand mr-0 {{ Route::current()->getName() == 'perfil' ? 'text-green' : 'text-grey' }}" href="{{ route('perfil') }}"><i class="fas fa-user"></i></a>
          </div>
        </div>
      </div>
      @endif
    </div>
  </div>
</div>