<nav class="navbar fixed-bottom navbar-dark bg-white">
    <a class="navbar-brand mr-0 {{ Route::current()->getName() == 'principal' || Route::current()->getName() == 'home' ? 'text-green' : 'text-grey' }}" href="{{ route('principal') }}"><i class="fas fa-home"></i></a>
    <div class="d-block">
        <a class="navbar-brand mr-0 {{ Route::current()->getName() == 'citas' ? 'text-green' : 'text-grey' }}" href="{{ route('citas') }}"><i class="fas fa-bell"></i>
            <span class="js-notificaciones position-absolute"></span>
        </a>
    </div>
    <div class="d-block">
        <a class="navbar-brand mr-0 {{ Route::current()->getName() == 'chat' ? 'text-green' : 'text-grey' }}" href="{{ route('chat') }}"><i class="fas fa-comment"></i>
            <span class="js-notificaciones-2 position-absolute"></span>
        </a>
    </div>
    <a class="navbar-brand mr-0 {{ str_contains(Route::current()->getName(), 'perfil') ? 'text-green' : 'text-grey' }}" href="{{ route('perfil') }}"><i class="fas fa-user"></i></a>
</nav>
