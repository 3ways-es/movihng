<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width">
<title>MOVIhNG</title>
@laravelPWA
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}?x={{ rand(5, 15) }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/constantes.css') }}?x={{ rand(5, 15) }}">
<link rel="icon" type="image/png" href="/images/favicon.png">
 @yield('styles')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}?x={{ rand(5, 15) }}" defer></script><script src="https://kit.fontawesome.com/43eb761e36.js" crossorigin="anonymous"></script>
<!-- <script type="text/javascript" src="{{ asset('js/cssrefresh.js') }}"></script> -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@yield('scripts')

