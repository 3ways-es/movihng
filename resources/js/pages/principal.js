/* FUNCION PARA LOS POPUPS*/
$(function() {
	$('.js-seccion').on('click', function(){
		let id = $(this).data('id')
		let color = $(this).data('color')
		if(id == 1)
		{
			cambiarSeccion(1,2, color)
		}
		else{
			cambiarSeccion(2,1, color)
		}
		  
	})

	function cambiarSeccion(idPulsado, idActivo, sColor){
		if($('.seccion-'+sColor+'-'+idPulsado).hasClass('d-none'))
		{
			$('.seccion-'+sColor+'-'+idActivo).addClass('d-none');
			$('.seccion-'+sColor+'-'+idPulsado).removeClass('d-none');
			$('.contenido-seccion-'+sColor+'-'+idActivo).fadeOut( "slow", function() {
				$('.contenido-seccion-'+sColor+'-'+idPulsado).fadeIn( "slow");
			});
		}
	}

	/* FUNCION PARA PLANIFICACION MENSUAL*/
	$('.js-mes').on('click', function(){
		// Almacenamos el mes a mostrar
		let programa = $(this).data('programa');
		let mes = $(this).data('mes');

		// Comprobamos que no esté activo
		if(!$('.js-mesactivo-'+programa+'-'+mes).hasClass('meses-activo'))
		{
			// Comprobamos quien tiene el mes activo
			let mesActivo = 0;

			for(var i = 1; i <= 3; i++)
			{
				if($('.js-mesactivo-'+programa+'-'+i).hasClass('meses-activo'))
				{
					mesActivo = i;
				}
			}

			// Intercambiamos el mes
			$('.js-mesactivo-'+programa+'-'+mes).addClass('meses-activo ')
			$('.js-mesactivo-'+programa+'-'+mesActivo).removeClass('meses-activo ')
			$('.js-mes-'+programa+'-'+mesActivo).fadeOut( "slow", function() {
				$('.js-mes-'+programa+'-'+mes).fadeIn( "slow");
			});
		}
	})
		
})