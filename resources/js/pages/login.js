$(function() {
	$('.btn-enviar').on('click', function(){
		if(!$(this).hasClass('comprobado'))
		{
			if($('#codigo').val().length)
			{
				$('.text-error').text('')
				$.ajax({
		          url:'ajax/checkmail',
		          data:{
		          	'codigo':$('#codigo').val()
		          },
		          type:'post',
		          success: function (response) {
		          	if(response['b_error'] == 0)
		          	{
		          		$('.js-primerpaso').remove();
		          		$('.js-carga').append(response['vista'])
		          	}
		          	else
		          	{
		          		$('.text-error').text(response['s_error'])
		          	}
		          },
		          statusCode: {
		             404: function() {
		                console.log('error 404')
		             }
		          },
		          error:function(x,xs,xt){
		              
		          }
		       	});
			}
			else{
				$('.text-error').text('Introduce un código')
			}
		  
		}
	})

	$('.btn-reset-password').on('click', function(){
		if($('#codigoreset').val().length)
		{
			$('.text-error').text('')
				$.ajax({
		          url:'ajax/sendresetpassword',
		          data:{
		          	'codigo':$('#codigoreset').val()
		          },
		          type:'post',
		          success: function (response) {
		          	if(response['b_error'] == 0)
		          	{
		          		$('.js-form').addClass('d-none')
		          		$('.js-respuestareset').removeClass('d-none');
		          	}
		          	else
		          	{
		          		$('.text-error-reset').text(response['s_error'])
		          	}
		          },
		          statusCode: {
		             404: function() {
		                console.log('error 404')
		             }
		          },
		          error:function(x,xs,xt){
		              
		          }
		       	});		  
		}
		else{
			$('.text-error-reset').text('Introduce un código')
		}

	})
})