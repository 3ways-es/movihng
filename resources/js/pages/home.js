$(function() {
	$('.circulo').on('click', function(){
		let boton = $(this).data('tipo')
		if($('.'+boton).hasClass('noactivo')){
			if(boton == 'principal')
			{
				$('.'+boton).removeClass('noactivo');
				$('.secundaria').removeClass('activo');
				$('.'+boton).addClass('activo');
				$('.secundaria').addClass('noactivo');

				$('#bloque-texto').fadeOut( "slow", function(){
					$('#logoprincipal').fadeIn( "slow");
				});
			}
			else{
				$('.'+boton).removeClass('noactivo');
				$('.principal').removeClass('activo');
				$('.'+boton).addClass('activo');
				$('.principal').addClass('noactivo');
				$('.contenedor-global').addClass('bloque-texto-objetivos');
				$('#logoprincipal').fadeOut( "slow", function(){
					$('#bloque-texto').fadeIn( "slow");
				});
			}
		}
	})
})