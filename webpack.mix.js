const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/pages/principal.js', 'public/js')
mix.js('resources/js/pages/perfil.js', 'public/js')
mix.js('resources/js/pages/navbar.js', 'public/js')
mix.js('resources/js/cssrefresh.js', 'public/js')

mix.js('resources/js/app.js', 'public/js')
  .postCss('resources/css/app.css', 'public/css', [
    //
  ])
  .postCss('resources/css/constantes.css', 'public/css', [
    //
  ])
  .postCss('resources/css/principal.css', 'public/css', [
    //
  ])
  .postCss('resources/css/perfil.css', 'public/css', [
    //
  ])
  .vue()